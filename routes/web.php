<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/current', 'HomeController@current')->name('current');

Route::get('/resetpassword', 'ResetPasswordController@store');

Route::post('/addPermohonan', 'PermohonanController@addPermohonan');
Route::post('/savePermohonan', 'PermohonanController@savePermohonan');

Route::get('/bookingExceeded', 'PermohonanController@bookingExceeded')->name('bookingExceeded');

Route::get('/kehadiran/{tgl_wawancara?}/{filter?}', 'PetugasController@presence')->name('kehadiran');
Route::post('/konfirmasiKehadiran', 'PetugasController@verifyPresence')->name('konfirmasiKehadiran');


Route::get('/viewverification', 'PermohonanController@showVerification')->name('viewverification');
Route::get('/viewinterview', 'PermohonanController@showInterview')->name('viewinterview');
Route::get('/viewissued', 'PermohonanController@showIssued')->name('viewissued');

//*** VERIFIKASI DOKUMEN PEMOHON ***//
Route::get('/verdok/{tgl_wawancara?}/{filter?}', 'PetugasController@applicantDocumentList')->name('verdok');
Route::post('/dokumenPemohon', 'PetugasController@applicantDocument')->name('dokumenPemohon');
Route::post('/verifikasiDokumen', 'PetugasController@verifyDocument')->name('verifikasiDokumen');

//*** VERIFIKASI DATA PEMOHON TOTAL ***//
Route::get('/verdata/{tgl_wawancara?}/{filter?}', 'PetugasController@applicantDataList')->name('verdata');
Route::post('/dataPemohon', 'PetugasController@applicantData')->name('dataPemohon');
Route::post('/verifikasiData', 'PetugasController@verifyData')->name('verifikasiData');

//*** LOKASI CONTROLLER ***//
Route::get('api/get-kabupaten-list','LokasiController@getKabupatenList');
Route::get('api/get-kecamatan-list','LokasiController@getKecamatanList');
Route::get('api/get-kelurahan-list','LokasiController@getKelurahanList');


Route::get('/pdf', function () {
    return view('formpdf');
});

Route::post('/submitForm','UserDetailController@store');

Route::get('/index','UserDetailController@index');

// Route::get('/downloadPDF','PermohonanController@downloadPDF')->name('downloadPDF');
Route::post('/downloadPDF','PermohonanController@downloadPDF')->name('downloadPDF');
Route::get('/downloadSummaryPDF','PermohonanController@downloadSummaryPDF')->name('downloadSummaryPDF');

Route::get('/riwayat', 'HomeController@riwayat')->name('riwayat');

Route::get('/templatePenolakanVerdok', 'SettingController@templatePenolakanVerdok')->name('templatePenolakanVerdok');
Route::get('/templatePenolakanVerdata', 'SettingController@templatePenolakanVerdata')->name('templatePenolakanVerdata');
Route::get('/templateUndanganWawancara', 'SettingController@templateUndanganWawancara')->name('templateUndanganWawancara');
Route::get('/templateSuratRekomendasi', 'SettingController@templateSuratRekomendasi')->name('templateSuratRekomendasi');
Route::post('/saveTemplate','SettingController@saveTemplate')->name('saveTemplate'); 


Route::get('/kuota', 'SettingController@kuota')->name('kuota');

Route::get('/signature', 'SettingController@signature')->name('signature');
Route::post('/signature_add', 'SettingController@signature_add')->name('signature_add');
Route::get('/signature_edit/{id}', 'SettingController@signature_edit')->name('signature_edit');
Route::get('/signature_destroy/{id}', 'SettingController@signature_destroy')->name('signature_destroy');

Route::post('downloadDokumenPemohon', 'PermohonanController@getLampiranPemohon');

Route::get('/contohPDF/{parameter}','SettingController@contohPDF')->name('contohPDF');

//*** CAPTCHA ***//

Route::get('refresh_captcha', 'HomeController@refreshCaptcha')->name('refresh_captcha');