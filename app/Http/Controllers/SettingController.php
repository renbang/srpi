<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Template;
use App\Models\Signature;
use PDF;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    

    
    public function templatePenolakanVerdok()
    {
        $summernote = Template::find(1);
        $ttd = Signature::getSignature(1);
        $listsign = Signature::getMsSignature();

        return view('setting/templatePenolakanVerdok', [
            'summernote' => $summernote,
            'listsign' => $listsign,
            'ttd' => $ttd,
        ]);
    }

    public function templatePenolakanVerdata()
    {
        $summernote = Template::find(2);
        $ttd = Signature::getSignature(2);
        $listsign = Signature::getMsSignature();

        return view('setting/templatePenolakanVerdata', [
            'summernote' => $summernote,
            'listsign' => $listsign,
            'ttd' => $ttd,
        ]);
    }

    public function templateUndanganWawancara()
    {
        $summernote = Template::find(3);
        $ttd = Signature::getSignature(3);
        $listsign = Signature::getMsSignature();

        return view('setting/templateUndanganWawancara', [
            'summernote' => $summernote,
            'listsign' => $listsign,
            'ttd' => $ttd,
        ]);
    }

    public function templateSuratRekomendasi()
    {
        $summernote = Template::find(4);
        $ttd = Signature::getSignature(4);
        $listsign = Signature::getMsSignature();

        return view('setting/templateSuratRekomendasi', [
            'summernote' => $summernote,
            'listsign' => $listsign,
            'ttd' => $ttd,
        ]);
    }

    public function saveTemplate(Request $request)
    {
        $template_id = $request->template_id;
        $header = $request->header;
        $nama=$request->template_nama;
        $detail=$request->template;
        $detail_footer=$request->template_footer;
        $signature=$request->signature;

        /*isi nya*/
        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
 
        $images = $dom->getelementsbytagname('img');
 
        foreach($images as $k => $img){
            $data = $img->getattribute('src');
            
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
 
            $data = base64_decode($data);
            $image_name= time().$k.'.png';
            $path = public_path() .'/'. $image_name;
            //dd($path);
            file_put_contents($path, $data);
 
            $img->removeattribute('src');
            $img->setattribute('src', $image_name);
        }
 
        $detail = $dom->savehtml();


        /*footer nya*/
        $dom_f = new \domdocument();
        $dom_f->loadHtml($detail_footer, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
 
        $detail_footer = $dom_f->savehtml();



        $tmp = DB::table('ms_template')->where('id', $template_id)
            ->update([                
                'template' => $detail,
                'template_footer' => $detail_footer,
                'header' => $header,
                'signature' => $signature
            ]);

         
        return redirect()->back();
    }

    public function contohPDF($parameter){

      
      $template = Template::find($parameter);
      $ttd = Signature::getSignature($parameter);

      //pdf : halaman
      // user : data
      $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('pdf/exPdf', compact(['template','ttd']));  
      return $pdf->download('example.pdf');

    }

    public function kuota()
    {
        return view('setting/kuota');
    }

    public function signature()    {
        $sign = null;
        $listsign = Signature::getListSignature();

        return view('setting/signature',['sign' => $sign,'listsign' => $listsign]);
    }

    public function signature_add(Request $request)
    {

        $existsignature = Signature::getSignatureById($request->id_sign);

        if(!$existsignature){

            if($request->hasFile('signature')){
                $uploadedFile = $request->file('signature');  
                $path = $uploadedFile->store('public/files/signature');
                $filefoto_name = $uploadedFile->getClientOriginalName();
            }
            $master_signature = Signature::create([
                'jabatan' => $request->jabatan,
                'nama_pejabat' => $request->nama_pejabat,
                //'link_signature' => 'storage/app/signature/'.$path,
                'link_signature' => $path,
            ]);
            //$permohonan_foto_insertedId = $permohonan_foto->id; 
        }
        else{
           if($request->hasFile('signature')){
                $uploadedFile = $request->file('signature');  
                $path = $uploadedFile->store('public/files/signature');
                $filefoto_name = $uploadedFile->getClientOriginalName();
                //$link_signature = 'storage/app/signature/'.$path;
                $link_signature = $path;
            }
            else { 
                $link_signature = $request->link_signature;
            }
            $master_signature = DB::table('ms_signature')->where('id', $request->id_sign)
            ->update([
                'jabatan' => $request->jabatan,
                'nama_pejabat' => $request->nama_pejabat,
                'link_signature' => $link_signature,
            ]);
        }

           
       
        return redirect('/signature');
    }

    public function signature_edit($id)
    {
        
        $sign = Signature::getSignatureById($id);

        $listsign = Signature::getListSignature();
        return view('setting/signature',['sign' => $sign, 'listsign' => $listsign]);

    }

    public function signature_destroy($id)
    {
        //delete
        $crud = Signature::getSignatureById($id);
        DB::table('ms_signature')
        ->where('id',$id)
        ->delete();


        $sign = null;
        $listsign = Signature::getListSignature();
        return redirect('signature');

    }
    
}
