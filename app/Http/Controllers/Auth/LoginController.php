<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\Biodata;
use App\Models\Permohonan;
use App\Models\PermohonanActivity;
use App\Models\Workflow;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected function authenticated(Request $request, $user)
    {
        $id_user = Auth::id();
        $user = Auth::user();

        if ($user->allow_login == 'Y')
        {
            $step = Permohonan::getActiveApplication($id_user);
               
            if($step){
                $linkredirect = Workflow::getMsWorkflow($step->id_workflow);
                $n = array(1);
                if (!in_array($step->id_workflow, $n)) {
                     return redirect($linkredirect->route);
                }   
            }     
            return redirect('/home');
        }
        elseif ($user->allow_login == 'N')
        {
            $activity = PermohonanActivity::getActivityByUser($id_user, 1);

            $activity_datetime = \Carbon\Carbon::parse($activity->activity_at);
            $current_datetime = \Carbon\Carbon::now();
            $difference = $current_datetime->diffInMinutes($activity_datetime);
            $remaining_time = $current_datetime->diff($activity_datetime)->format('%H:%i:%s');

            if ($difference > 120)
            {
                $allow_login = DB::table('users')
                              ->where('id', '=', $id_user)
                              ->update(['allow_login' => 'Y']);

                $delete_booking_activity = PermohonanActivity::deleteActivityNullTiketByUser($id_user, 1);
                
                $step = Permohonan::getActiveApplication($id_user);
                if($step){
                    $linkredirect = Workflow::getMsWorkflow($step->id_workflow);
                    $n = array(1);
                    if (!in_array($step->id_workflow, $n)) {
                         return redirect($linkredirect->route);
                    }   
                }     
                return redirect('/home');
            }
            else
            {
                $this->guard()->logout();
                $request->session()->flush();
                $request->session()->regenerate();
                $user_message = 'Anda terdeteksi sudah melakukan Apply Permohonan sebanyak 9 kali. Akun Anda dinonaktifkan selama ' . $remaining_time . ' terhitung sejak pukul ' . $activity->activity_at;
                return view('/auth/login', ['user_message' => $user_message]);
            } 
        }
        else
        {
            $this->guard()->logout();
            $request->session()->flush();
            $request->session()->regenerate();
            return view('/auth/login');
        }
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    // protected $redirectAfterLogout = '/login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    { 
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        $user_message = null;
        if ($request->user_message) $user_message = $request->user_message;
        return view('/auth/login', ['user_message' => $user_message]);
    }


    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
            'captcha' => 'required|captcha'
        ]);
    }


}
