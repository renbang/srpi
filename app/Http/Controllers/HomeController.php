<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Biodata;
use App\Models\Lokasi;
use App\Models\Permohonan;
use App\Models\PermohonanActivity;
use App\Models\Workflow;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_user = Auth::id();
        $user_message = null;

        $booking_count = PermohonanActivity::getBookingActivityCount($id_user);
        if($booking_count > 9)
        {
            $system_datetime = \Carbon\Carbon::now();
            $current_datetime  = $system_datetime->toDateTimeString();

            $disallow_login = DB::table('users')
                              ->where('id', '=', $id_user)
                              ->update(['allow_login' => 'N']);

            $user_message = 'Anda terdeteksi sudah melakukan Apply Permohonan sebanyak 9 kali. Akun Anda dinonaktifkan selama 2 jam terhitung sejak pukul ' . $current_datetime;
        }

        $kuota = DB::table('kuota_wawancara')->where('is_open','Y')->get();

        $sumpublished = DB::table('kuota_wawancara')
                     ->select(DB::raw('coalesce(sum(kuota_dibuka),0) as quota_published, coalesce(sum(kuota_diambil),0) as quota_taken, coalesce(sum(kuota_tersedia),0) as quota_available'))
                     ->where('is_open', '=', 'Y')
                     ->first();

        $sumunpublished = DB::table('kuota_wawancara')
                     ->select(DB::raw('coalesce(sum(kuota_dibuka),0) as quota_unopen'))
                     ->where('is_open', '=', 'N')
                     ->first();

        $id_user = Auth::id();
        $step = Permohonan::getActiveApplication($id_user);
        $id_workflow = 0;
       
        if ($step != null) {
            $workflow = Workflow::getMsWorkflow($step->id_workflow);
            $id_workflow = $workflow->id_workflow;
        } else {
            $id_workflow = 999;
        }

        return view('home', ['kuota' => $kuota,
                             'sumpublished' => $sumpublished,
                             'sumunpublished' => $sumunpublished,
                             'id_workflow' => $id_workflow,
                             'booking_count' => $booking_count,
                             'user_message' => $user_message ]);
    }

    public function riwayat()
    {
        $id_user = Auth::id();
        $biodata = Biodata::getBio();
        $lokasi=null;
        $application=null;
        if($biodata){
        $lokasi = Lokasi::getCompleteLokasi($biodata->kd_lokasi);
        $application = Permohonan::getHistoryApplication($id_user);
        }
        //dd($application);
        return view('history',['application' => $application, 'biodata' => $biodata, 'lokasi' => $lokasi]);
    }


    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }    

    public function current()
    {
        $id_user = Auth::id();
        $step = Permohonan::getActiveApplication($id_user);
        if($step){
            $linkredirect = Workflow::getMsWorkflow($step->id_workflow);
            $n = array(1);
            if (!in_array($step->id_workflow, $n)) {
                 return redirect($linkredirect->route);
            }   
        }     
        return redirect('/home');
    }

}
