<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Permohonan;
use App\Models\PermohonanActivity;
use App\Models\KuotaWawancara;
use App\Models\Workflow;
use App\Models\Lampiran;
use App\Models\Template;
use App\Models\Lokasi;
use App\Models\Pendidikan;
use PDF;

class PetugasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    //*** VERIFIKASI DOKUMEN PEMOHON, SETELAH TAHAP PENGISIAN FORM ***//
    public function applicantDocumentList($tgl_wawancara = null, $filter = null)
    {
        $list_tanggal = KuotaWawancara::getAllTanggalWawancara();
        
        if (!$tgl_wawancara)
        {
            $system_time = \Carbon\Carbon::now();
            $current_date = $system_time->toDateString();
            $selected_option = 0;
            $counter = 0;
            
            foreach ($list_tanggal as $tanggal)
            {
                if ($tanggal->tgl_wawancara >= $current_date && $selected_option == 0) { 
                    $selected_option = 1;
                    $tgl_wawancara = $tanggal->tgl_wawancara;
                } elseif ($tanggal->tgl_wawancara < $current_date && $selected_option == 0) {
                    if ($counter+1 == count($list_tanggal) && $selected_option == 0) {
                        $selected_option = 1;
                        $tgl_wawancara = $tanggal->tgl_wawancara;
                    }
                }
                $counter++;
            }
        }

        $verifikator = Auth::user()->name;

        $list_dokumen = Permohonan::getApplicantDocumentList($tgl_wawancara);

        $count_belum_diverifikasi = $list_dokumen->where('status', null)->count();
        $count_diterima = $list_dokumen->where('status', 'Y')->count();
        $count_ditolak = $list_dokumen->where('status', 'N')->count();
        $count_total = $list_dokumen->count();

        return view('/petugas/applicantDocumentList',
                   ['list_dokumen' => $list_dokumen,
                    'list_tanggal' => $list_tanggal,
                    'tgl_wawancara' => $tgl_wawancara,
                    'verifikator' => $verifikator,
                    'count_belum_diverifikasi' => $count_belum_diverifikasi,
                    'count_diterima' => $count_diterima,
                    'count_ditolak' => $count_ditolak,
                    'count_total' => $count_total]);
    }

    public function applicantDocument(Request $request)
    {
        $dokumen = Permohonan::getApplicantDocument($request->no_tiket);
        $ms_lampiran = Lampiran::getMasterLampiran();
        $list_lampiran = Lampiran::getApplicantAttachment($request->no_tiket);
        $activity = PermohonanActivity::getActivityByIdWorkflow($request->no_tiket, 3);

        return view('/petugas/applicantDocument',
            ['dokumen' => $dokumen,
             'ms_lampiran' => $ms_lampiran,
             'list_lampiran' => $list_lampiran,
             'no_tiket' => $request->no_tiket,
             'activity' => $activity
            ]);
    }

    public function verifyDocument(Request $request)
    {
        $id_user = $request->id_user;
        $id_kuota_wawancara = $request->id_kuota_wawancara;
        $no_tiket = $request->no_tiket;
        $id_workflow = 3;
        if ($request->status == "Y") {            
            $status = "Y";
            $template = Template::getTemplateByIdWorkflow($id_workflow, $status);
            $keterangan = null;
            $update_status = Permohonan::updateApplicationStatus($no_tiket, 4);
            $new_activity = PermohonanActivity::createActivity($id_user, $id_kuota_wawancara, $no_tiket, $id_workflow, $status, $template->id, $keterangan);
        } elseif ($request->status == "N") {
            $status = "N";
            $template = Template::getTemplateByIdWorkflow($id_workflow, $status);
            $keterangan = $request->keterangan;
            $new_activity = PermohonanActivity::createActivity($id_user, $id_kuota_wawancara, $no_tiket, $id_workflow, $status, $template->id, $keterangan);
        }

        return redirect()->action('PetugasController@applicantDocumentList', $request->tgl_wawancara);
    }



    //*** KEHADIRAN WAWANCARA ***//
    public function presence($tgl_wawancara = null, $filter = null)
    {
        $list_tanggal = KuotaWawancara::getAllTanggalWawancara();
        
        if (!$tgl_wawancara)
        {
            $system_time = \Carbon\Carbon::now();
            $current_date = $system_time->toDateString();
            $selected_option = 0;
            $counter = 0;
            
            foreach ($list_tanggal as $tanggal)
            {
                if ($tanggal->tgl_wawancara >= $current_date && $selected_option == 0) { 
                    $selected_option = 1;
                    $tgl_wawancara = $tanggal->tgl_wawancara;
                } elseif ($tanggal->tgl_wawancara < $current_date && $selected_option == 0) {
                    if ($counter+1 == count($list_tanggal) && $selected_option == 0) {
                        $selected_option = 1;
                        $tgl_wawancara = $tanggal->tgl_wawancara;
                    }
                }
                $counter++;
            }
        }

        $petugas = Auth::user()->name;

        $list_kehadiran = Permohonan::getPresenceData($tgl_wawancara);

        $count_belum_hadir = $list_kehadiran->where('id_workflow', '<=', 4)->count();
        $count_sudah_hadir = count($list_kehadiran) - $count_belum_hadir;
        $count_total = count($list_kehadiran);

        return view('/petugas/presence',
                   ['list_kehadiran' => $list_kehadiran,
                    'list_tanggal' => $list_tanggal,
                    'tgl_wawancara' => $tgl_wawancara,
                    'petugas' => $petugas,
                    'count_belum_hadir' => $count_belum_hadir,
                    'count_sudah_hadir' => $count_sudah_hadir,
                    'count_total' => $count_total ]);
    }

    public function verifyPresence(Request $request)
    {
        $id_user = $request->id_user;
        $id_kuota_wawancara = $request->id_kuota_wawancara;
        $no_tiket = $request->no_tiket;
        $id_workflow = 4;

        $update_status = Permohonan::updateApplicationStatus($request->no_tiket, 5);
        $new_activity = PermohonanActivity::createActivity($id_user, $id_kuota_wawancara, $no_tiket, $id_workflow, null, null, null);

        return redirect()->action('PetugasController@presence', $request->tgl_wawancara);
    }



    //*** VERIFIKASI DATA KESELURUHAN PEMOHON, MULAI DARI AWAL SAMPAI SETELAH WAWANCARA ***//
    public function applicantDataList($tgl_wawancara = null, $filter = null)
    {
        $list_tanggal = KuotaWawancara::getAllTanggalWawancara();
        
        if (!$tgl_wawancara)
        {
            $system_time = \Carbon\Carbon::now();
            $current_date = $system_time->toDateString();
            $selected_option = 0;
            $counter = 0;
            
            foreach ($list_tanggal as $tanggal)
            {
                if ($tanggal->tgl_wawancara >= $current_date && $selected_option == 0) { 
                    $selected_option = 1;
                    $tgl_wawancara = $tanggal->tgl_wawancara;
                } elseif ($tanggal->tgl_wawancara < $current_date && $selected_option == 0) {
                    if ($counter+1 == count($list_tanggal) && $selected_option == 0) {
                        $selected_option = 1;
                        $tgl_wawancara = $tanggal->tgl_wawancara;
                    }
                }
                $counter++;
            }
        }

        $verifikator = Auth::user()->name;

        $list_data = Permohonan::getApplicantDataList($tgl_wawancara);

        $count_diterima = $list_data->where('id_workflow', '6')->count();
        $count_ditolak = $list_data->where('status', 'N')->count();
        $count_total = $list_data->count();
        $count_belum_diverifikasi = $count_total - ($count_diterima + $count_ditolak);

        return view('/petugas/applicantDataList',
                   ['list_data' => $list_data,
                    'list_tanggal' => $list_tanggal,
                    'tgl_wawancara' => $tgl_wawancara,
                    'verifikator' => $verifikator,
                    'count_belum_diverifikasi' => $count_belum_diverifikasi,
                    'count_diterima' => $count_diterima,
                    'count_ditolak' => $count_ditolak,
                    'count_total' => $count_total]);
    }

    public function applicantData(Request $request)
    {
        $data = Permohonan::getApplicantData($request->no_tiket);
        $ms_lampiran = Lampiran::getMasterLampiran();
        $list_lampiran = Lampiran::getApplicantAttachment($request->no_tiket);
        
        $id_user = $request->id_user;
        $id_kuota_wawancara = $request->id_kuota_wawancara;
        $no_tiket = $request->no_tiket;
        $id_workflow = 5;

        $check_activity = PermohonanActivity::getActivityByIdWorkflow($no_tiket, $id_workflow);
        if(!$check_activity)
        {
            $new_activity = PermohonanActivity::createActivity($id_user, $id_kuota_wawancara, $no_tiket, $id_workflow, null, null, null);
        }

        $activity = PermohonanActivity::getActivityByIdWorkflow($request->no_tiket, 5);

        $listkabupaten = null;
        $listkecamatan = null;
        $listkelurahan = null;
        $lokasi = null;
        if($data){
            $lokasi = Lokasi::getCompleteLokasi($data->kd_lokasi);
            if($lokasi){
                $listkabupaten = Lokasi::getKabupaten($lokasi->provinsi);
                $listkecamatan = Lokasi::getKecamatan($lokasi->kabupaten);
                $listkelurahan = Lokasi::getKelurahan($lokasi->kecamatan);
            }
        }
        $listpropinsi = Lokasi::getProvinsi();
        $liststatusmhs = Pendidikan::getStatusMhs();
        $listjenjang = Pendidikan::getJenjang();

        return view('/petugas/applicantData',
            ['data' => ($data)?$data:null,
             'lokasi' => ($lokasi)?$lokasi:null,
             'listpropinsi' => $listpropinsi,
             'listkabupaten' => $listkabupaten,
             'listkecamatan' => $listkecamatan,
             'listkelurahan' => $listkelurahan,
             'liststatusmhs' => $liststatusmhs,
             'listjenjang' => $listjenjang,
             'ms_lampiran' => $ms_lampiran,
             'list_lampiran' => $list_lampiran,
             'no_tiket' => $request->no_tiket,
             'activity' => $activity
            ]);
    }

    public function verifyData(Request $request)
    {
        $id_activity = $request->id_activity;
        $id_user = $request->id_user;
        $id_kuota_wawancara = $request->id_kuota_wawancara;
        $no_tiket = $request->no_tiket;
        $keterangan = $request->keterangan;
        $id_workflow = 6;
        if ($request->status == "Y") {            
            $status = "Y";
            $template = Template::getTemplateByIdWorkflow($id_workflow, $status);
            $update_activity = PermohonanActivity::updateActivity($id_activity, $status, null, null);
            $update_status = Permohonan::updateApplicationStatus($no_tiket, $id_workflow);
            $new_activity = PermohonanActivity::createActivity($id_user, $id_kuota_wawancara, $no_tiket, $id_workflow, null, $template->id, $keterangan);
        } elseif ($request->status == "N") {
            $status = "N";
            $template = Template::getTemplateByIdWorkflow(5, $status);
            $keterangan = $request->keterangan;
            $update_activity = PermohonanActivity::updateActivity($id_activity, $status, $keterangan, $template->id);
        }

        return redirect()->action('PetugasController@applicantDataList', $request->tgl_wawancara);
    }
}