<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Lokasi;
use App\Models\Pendidikan;
use App\Models\Biodata;
use App\Models\Permohonan;
use App\Models\PermohonanActivity;
use App\Models\PermohonanFile;
use App\Models\Lampiran;
use App\Models\Template;
use App\Models\Signature;

use Illuminate\Support\Facades\Auth;
use PDF;

class PermohonanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function addPermohonan(Request $request)
    {
        $id_user = Auth::id();
            
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();
        
        $new_activity = PermohonanActivity::create([
            'id_user' => $id_user,
            'id_kuota_wawancara' => $request->id_kuota_wawancara,
            'id_workflow' => 1,
            'activity_at' => $current_datetime,
            'activity_by' => $id_user,
        ]);
        $new_activity_insertedId = $new_activity->id;

        $booking_datetime = $current_datetime;

        $tanggal= $request->tanggal;
        $id_kuota_wawancara= $request->id_kuota_wawancara;
        
        $biodata = Biodata::getBio();
        $listkabupaten = null;
        $listkecamatan = null;
        $listkelurahan = null;
        $lokasi = null;
        if($biodata){
            $lokasi = Lokasi::getCompleteLokasi($biodata->kd_lokasi);
            if($lokasi){
                $listkabupaten = Lokasi::getKabupaten($lokasi->provinsi);
                $listkecamatan = Lokasi::getKecamatan($lokasi->kabupaten);
                $listkelurahan = Lokasi::getKelurahan($lokasi->kecamatan);
            }
        }
        $listpropinsi = Lokasi::getProvinsi();
        $liststatusmhs = Pendidikan::getStatusMhs();
        $listjenjang = Pendidikan::getJenjang();
        $listlampiran = Lampiran::getMsLampiran();
        
        return view('permohonan/add', [
            'biodata' => ($biodata)?$biodata:null,
            'lokasi' => ($lokasi)?$lokasi:null,
            'tanggal' => $tanggal,
            'id_kuota_wawancara' => $id_kuota_wawancara,
            'listpropinsi' => $listpropinsi,
            'listkabupaten' => $listkabupaten,
            'listkecamatan' => $listkecamatan,
            'listkelurahan' => $listkelurahan,
            'liststatusmhs' => $liststatusmhs,
            'listjenjang' => $listjenjang,
            'listlampiran'  => $listlampiran,
            'booking_datetime' => $booking_datetime,
            'id_activity' => $new_activity_insertedId
        ]);
    }

    public function savePermohonan(Request $request)
    {
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();
        
        $this->validate($request, [
            'nama' => 'required|max:50',
            'captcha' => 'required|captcha'
        ],
        [   
            'nama.required'    => 'This field must not be empty.',
            'captcha.required'      => 'Please fill with the correct word!',
        ]
        );

       
        $tgl_hbs_berlaku_ddmmyyyy= $request->tgl_hbs_berlaku;
        $tgl_hbs_berlaku_yyyyMMdd = date('Y-m-d', strtotime($tgl_hbs_berlaku_ddmmyyyy));
        
        $id_kuota_wawancara = $request->id_kuota_wawancara;

        $id_activity = $request->id_activity;

        $existbiodata = Biodata::getBio();

        if(!$existbiodata){
            $bio = Biodata::create([
                'id_user' => Auth::id(),
                'nama' => strtoupper($request->nama),
                'jns_kel' => strtoupper($request->jns_kel),
                'tmp_lahir' => strtoupper($request->tmp_lahir),
                'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                'nik' => $request->nik,
                'no_paspor' => strtoupper($request->no_paspor),
                'tgl_diterbitkan' => date('Y-m-d', strtotime($request->tgl_diterbitkan)),
                'tgl_hbs_berlaku' =>  date('Y-m-d', strtotime($request->tgl_hbs_berlaku)),
                'alamat' => strtoupper($request->alamat),
                'kd_lokasi' => $request->kelurahan,
                'email' => $request->email,
                'no_tlp' => $request->no_tlp,
            ]);
            $bio_insertedId = $bio->id;
        }
        else{
            $bio = DB::table('biodata')
            ->where('id_bio', $existbiodata->id_bio)
            ->update([
                'nama' => strtoupper($request->nama),
                'jns_kel' => strtoupper($request->jns_kel),
                'tmp_lahir' => strtoupper($request->tmp_lahir),  
                'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
                'nik' => $request->nik,
                'no_paspor' => strtoupper($request->no_paspor),
                'tgl_diterbitkan' => date('Y-m-d', strtotime($request->tgl_diterbitkan)),
                'tgl_hbs_berlaku' => date('Y-m-d', strtotime($request->tgl_hbs_berlaku)),
                'alamat' => strtoupper($request->alamat),
                'kd_lokasi' => $request->kelurahan,
                'email' => $request->email,
                'no_tlp' => $request->no_tlp,
            ]);
            $bio_insertedId = $existbiodata->id_bio;
        }

        $tgl_test_ddmmyyyy= $request->tgl_test;
        $tgl_test_yyyyMMdd = date('Y-m-d', strtotime($tgl_test_ddmmyyyy));


        $id_user = Auth::id();
        $cek = Permohonan::getActiveApplication($id_user);
        $no_tiket = null;
        if(!$cek) {
            //insert permohonan
            $permohonan = Permohonan::create([
                'id_bio' => $bio_insertedId,
                'id_user' => $id_user,
                'id_workflow' => 3,
                'id_kuota_wawancara' => $id_kuota_wawancara,
                'tmp_kuliah' => strtoupper($request->tmp_kuliah),
                'kd_status_mahasiswa' => $request->kd_status_mahasiswa,
                'kd_jenjang' => $request->kd_jenjang,
                'thn_lulus' => $request->thn_lulus,
                'nm_universitas' => strtoupper($request->nm_universitas),
                'alamat_universitas' => strtoupper($request->alamat_universitas),
                'fakultas' => strtoupper($request->fakultas),
                'jurusan' => strtoupper($request->jurusan),
                'tgl_test' => $tgl_test_yyyyMMdd,
                'no_identitas_kandidat' => strtoupper($request->no_identitas_kandidat),
                'skor' => $request->skor,
                'alasan_kedatangan' => strtoupper($request->alasan_kedatangan),
            ]);
            $permohonan_insertedId = $permohonan->id;
            $no_tiket = $permohonan->id;

            //insert permohonan_file
            $listlampiran = Lampiran::getMsLampiran();
            foreach ($listlampiran as $lampiran){
                if($request->hasFile($lampiran->id_lampiran))
                {
                    $uploadedFile = $request->file($lampiran->id_lampiran);  
                    $path = $uploadedFile->store('public/files/lampiran');
                    $filefoto_name = $uploadedFile->getClientOriginalName();
                    $permohonan_foto = PermohonanFile::create([
                        'no_tiket' => $permohonan_insertedId,
                        'kd_lampiran' => $lampiran->kd_lampiran,
                        'link' => $path,
                    ]);
                    $permohonan_foto_insertedId = $permohonan_foto->id; 
                }
            }

            $message = 'Your data have already stored in our database.
                Please wait, our officer is processing your documents. We will inform you the result at least 1 day before the interview date schedule.';
        }
        else {
             $tiket = DB::table('permohonan')->where('no_tiket', $cek->no_tiket)
            ->update([
                'id_workflow' => 3,
                'id_kuota_wawancara' => $id_kuota_wawancara,
                'tmp_kuliah' => strtoupper($request->tmp_kuliah),
                'kd_status_mahasiswa' => $request->kd_status_mahasiswa,
                'kd_jenjang' => $request->kd_jenjang,
                'thn_lulus' => $request->thn_lulus,
                'nm_universitas' => strtoupper($request->nm_universitas),
                'alamat_universitas' => strtoupper($request->alamat_universitas),
                'tgl_test' => $tgl_test_yyyyMMdd,
                'no_identitas_kandidat' => strtoupper($request->no_identitas_kandidat),
                'skor' => $request->skor,
                'alasan_kedatangan' => strtoupper($request->alasan_kedatangan),
            ]);           

            $message = 'You already have an existing active application. 
                Please wait, our officer is processing your previous document. We will inform you the result at least 1 day before the interview date schedule.';
        }

        $update_booking_activity = PermohonanActivity::updateActivityNoTiket($id_activity, $no_tiket);

        $delete_booking_activity = PermohonanActivity::deleteActivityNullTiketByUser($id_user, 1); //for null no_tiket

        $new_activity = PermohonanActivity::create([
            'id_user' => $id_user,
            'id_kuota_wawancara' => $id_kuota_wawancara,
            'no_tiket' => $no_tiket,
            'id_workflow' => 2,
            'activity_at' => $current_datetime,
            'activity_by' => $id_user,
        ]);

        $new_activity_insertedId = $new_activity->id;
        
        $biodata = Biodata::getBio();
        
        if($biodata){
            $lokasi = Lokasi::getCompleteLokasi($biodata->kd_lokasi);
            $activeapp = Permohonan::getCompleteDataApplication($id_user);
        }

        $activity = PermohonanActivity::getActivityByIdWorkflow($no_tiket, 3);

        $user = Auth::user();

        return view('permohonan/viewverification', [
            'nama'=> $user->name,
            'message' => $message,
            'biodata' => $biodata,
            'lokasi' => $lokasi,
            'activeapp' => $activeapp,
            'activity' => $activity
        ]);
    }

    public function showVerification(Request $request)
    {
        $user = Auth::user();

        $lokasi = null;
        $application = Permohonan::getDataApplication($user->id);
        
        $biodata = Biodata::getBio();

        if($biodata){
            $lokasi = Lokasi::getCompleteLokasi($biodata->kd_lokasi);
            $activeapp = Permohonan::getCompleteDataApplication($user->id);
        }

        $activity = PermohonanActivity::getActivityByIdWorkflow($application->no_tiket, 3);

        if($activity) {
            $message = 'Your application has already verified. See the result below on the "STATUS" section.';
        } else {
            $message = 'You already have an existing active application. 
                Please wait, our officer is processing your previous document. We will inform you the result at least 1 day before the interview date schedule.';
        }

        return view('permohonan/viewverification', [
            'nama'=> $user->name,
            'message' => $message,
            'biodata' => $biodata,
            'lokasi' => $lokasi,
            'activeapp' => $activeapp,
            'activity' => $activity
        ]);
    }

    public function showInterview(Request $request)
    {
        $user = Auth::user();
        $application = Permohonan::getDataApplication($user->id);

        $activity = PermohonanActivity::getActivityByIdWorkflow($application->no_tiket, 4);
        $activity_wawancara = null;
        $message = '';
        $flag = 0;

        if($activity) {
            $activity_wawancara = PermohonanActivity::getActivityByIdWorkflow($application->no_tiket, 5);
            if($activity_wawancara) {
                if($activity_wawancara->status == "Y") {
                    $message = 'Big congratulations! You have passed all the steps completely from the beginning to the end. Please download this file to get your recommedation letter (E-SRPI). Once again, congrats!';
                    $flag = 99;
                } elseif($activity_wawancara->status == "N") {
                    $message = 'We are sorry to you, you are rejected. Please refer to this remark and rejection letter.';
                    $flag = -99;
                }
            } else {
                $message = 'You attend the Interview.';
                $flag = 1;
            }
        } else {
            $system_datetime = \Carbon\Carbon::now();
            $current_date  = $system_datetime->toDateString();
            
            if($current_date >= $application->tgl_wawancara) {
                $message = 'You have failed all the process because you did not come to the Interview on the arranged schedule.';
                $flag = -1;
            } else {
                $message = 'Congratulations your document is passed the verification process! Please download the Interview Invitation Letter and bring all of the required documents on the Interview Schedule.';
                $flag = 0;
            }
        }

        return view('permohonan/viewinterview', [
            'message' => $message,
            'flag' => $flag,
            'application' => $application,
            'activity' => ($activity)?$activity:null,
            'activity_wawancara' => ($activity_wawancara)?$activity_wawancara:null
        ]);
    }

    public function showIssued(Request $request)
    {
        $user = Auth::user();
        $application = Permohonan::getDataApplication($user->id);

        $activity = PermohonanActivity::getActivityByIdWorkflow($application->no_tiket, 6);
        $message = 'Big congratulations! You have passed all the steps completely from the beginning to the end. Please download this file to get your recommedation letter (E-SRPI). Once again, congrats!';


        return view('permohonan/viewissued', [
            'message' => $message,
            'activity' => $activity
        ]);
    }


    public function downloadPDF(Request $request){

        $id_user = Auth::id();
        $application = Permohonan::getDataApplication($id_user);

        $activity = PermohonanActivity::getActivityByIdWorkflow($application->no_tiket, $request->id_workflow);
        
        $parameter = $activity->id_template;
        $_template = Template::find($parameter);

        $header = $_template->header;
        $ttd = Signature::getSignature($parameter);

        $template_footer = $_template->template_footer;


        $template = str_replace('&lt;nama&gt;',             ($application->nama             )?ucwords(strtolower($application->nama))            :'-', $_template->template);
        $template = str_replace('&lt;no_tiket&gt;',         ($application->no_tiket         )?str_pad($application->no_tiket,6,"0",STR_PAD_LEFT):'-', $template);
        $template = str_replace('&lt;jns_kel&gt;',          ($application->jns_kel          )?$application->jns_kel         :'-', $template);
        $template = str_replace('&lt;tmp_lahir&gt;',        ($application->tmp_lahir        )?ucwords(strtolower($application->tmp_lahir))       :'-', $template);
        $template = str_replace('&lt;tgl_lahir&gt;',        ($application->tgl_lahir        )?\Carbon\Carbon::parse($application->tgl_lahir)->format('d F Y')      :'-', $template);
        $template = str_replace('&lt;nik&gt;',              ($application->nik              )?$application->nik             :'-', $template);
        $template = str_replace('&lt;no_paspor&gt;',        ($application->no_paspor        )?$application->no_paspor       :'-', $template);
        $template = str_replace('&lt;alamat&gt;',           ($application->alamat           )?ucwords(strtolower($application->alamat))          :'-', $template);
        $template = str_replace('&lt;kelurahan&gt;',        ($application->kelurahan        )?ucwords(strtolower($application->kelurahan))       :'', $template);
        $template = str_replace('&lt;kecamatan&gt;',        ($application->kecamatan        )?ucwords(strtolower($application->kecamatan))       :'', $template);
        $template = str_replace('&lt;kabupaten&gt;',        ($application->kabupaten        )?ucwords(strtolower($application->kabupaten))       :'', $template);
        $template = str_replace('&lt;provinsi&gt;',         ($application->provinsi         )?ucwords(strtolower($application->provinsi))        :'', $template);
        $template = str_replace('&lt;kodepos&gt;',          ($application->kodepos          )?$application->kodepos         :'', $template);
        $template = str_replace('&lt;desc_jenjang&gt;',     ($application->desc_jenjang     )?ucwords(strtolower($application->desc_jenjang))    :'-', $template);
        $template = str_replace('&lt;nm_universitas&gt;',   ($application->nm_universitas   )?ucwords(strtolower($application->nm_universitas))  :'-', $template);
        $template = str_replace('&lt;keterangan&gt;',     ($activity->keterangan     )?$activity->keterangan    :'-', $template);
        $template = str_replace('&lt;hari_wawancara&gt;',   ($application->tgl_wawancara    )?\Carbon\Carbon::parse($application->tgl_wawancara)->format('l')   :'-', $template);
        $template = str_replace('&lt;tgl_wawancara&gt;',    ($application->tgl_wawancara    )?\Carbon\Carbon::parse($application->tgl_wawancara)->format('d F Y')    :'-', $template);
        // $template = str_replace('&lt;pukul_wawancara&gt;',  ($application->tgl_wawancara    )?\Carbon\Carbon::parse($application->tgl_wawancara)->format('H:i')   :'-', $template);
        $template = str_replace('&lt;pukul_wawancara&gt;',  ($application->tgl_wawancara    )?'09:00':'-', $template);
        $template = str_replace('&lt;datenow&gt;', date("d-m-Y"), $template);
        
        //pdf : halaman
        // user : data
        $pdf = PDF::loadView('pdf/pdf', compact(['application','header','template','template_footer','ttd']));  
        return $pdf->download('res_'.$application->no_tiket.'.pdf');

    }

    public function downloadSummaryPDF(){

        $id_user = Auth::id();
        $application = Permohonan::getDataApplication($id_user);
        
        $biodata = Biodata::getBio();
        $lokasi = null;
        $activeapp = null;
        if($biodata){
            $lokasi = Lokasi::getCompleteLokasi($biodata->kd_lokasi);
            $activeapp = Permohonan::getCompleteDataApplication($id_user);
        }

        //pdf : halaman
        // user : data
        $pdf = PDF::loadView('pdf/summaryPdf', compact(['application','biodata','lokasi','activeapp']));  
        return $pdf->download('sum_'.$application->no_tiket.'.pdf');
    }

    public function getLampiranPemohon(Request $request)
    {
        $renamedPath = str_replace('/', '\\', $request->link);
        $filePath = storage_path('app\\' . $renamedPath);
        if (! file_exists($filePath)) {
            // Some response with error message...
        }
        return response()->download($filePath);
    }
}
