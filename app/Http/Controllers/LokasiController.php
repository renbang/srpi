<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Lokasi;


class LokasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getKabupatenList(Request $request)
    {
        $listkabupaten = Lokasi::getKabupaten($request->provinsi);  
        //dd($listkabupaten);
        return response()->json($listkabupaten);
    }

     public function getKecamatanList(Request $request)
    {
        $listkecamatan = Lokasi::getKecamatan($request->kabupaten);  
        
        return response()->json($listkecamatan);
    }

     public function getKelurahanList(Request $request)
    {
        $listkelurahan = Lokasi::getKelurahan($request->kecamatan);  
        
        return response()->json($listkelurahan);
    }
}