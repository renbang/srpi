<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Workflow extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $table = 'ms_workflow';
  
    protected $guarded = ['id_workflow'];

    public static function getMsWorkflow($id_workflow)
    {
       $workflow = DB::table('ms_workflow as a')->select('a.*')
       ->where('id_workflow','=',$id_workflow)
       ->orderBy('id_workflow', 'desc')
            ->first();
        return $workflow;
    }
   

}
