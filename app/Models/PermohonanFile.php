<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermohonanFile extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    public $table = 'permohonan_file';
  
    protected $guarded = ['id','created_at','updated_at'];


   
}
