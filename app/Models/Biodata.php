<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Biodata extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    public $table = 'biodata';
  
   protected $guarded = [];


   public static function getBio()
   {
      $id_user = Auth::id();
       $bio = DB::table('biodata')->where('id_user','=',$id_user)
        ->first();
        return $bio;
   }

   
}
