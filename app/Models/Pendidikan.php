<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\DB;
class Pendidikan extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];


   //public $timestamps = false; 

   //whitelist
   //protected $fillable = ['title','description'];

   //blacklist
   //protected $guarded = ['id','created_at'];
  
   //klo ga mau ngelist whitelist, bikin guarded kosongan aja
   protected $guarded = [];

   public static function getJenjang()
   {
       $listjenjang = DB::table('ms_jenjang')->distinct()->select('kd_jenjang', 'desc_jenjang')
         ->orderBy('kd_jenjang', 'asc')->get();

        return $listjenjang;
   }
   
   public static function getStatusMhs()
   {
       $liststatusmhs = DB::table('ms_status_mahasiswa')->distinct()->select('kd_status_mahasiswa', 'desc_status_mahasiswa')
         ->orderBy('kd_status_mahasiswa', 'asc')->get();

        return $liststatusmhs;
   }
}
