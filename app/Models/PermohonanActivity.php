<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PermohonanActivity extends Model
{
	use SoftDeletes;
    public $table = 'permohonan_activity';
    protected $guarded = ['id_activity'];
    public $timestamps = false;

	public static function getActivityByUser($id_user, $id_workflow)
	{
		$act = DB::table('permohonan_activity')
		  ->where('id_user', '=', $id_user)
		  ->where('id_workflow', '=', $id_workflow)
		  ->orderByRaw('id_activity DESC')
		  ->first();
		if($act)
		  return $act;
		else
		  return null;
	}

	public static function getActivityByIdWorkflow($no_tiket, $id_workflow)
	{
		$act = DB::table('permohonan_activity AS pa')
			->leftJoin('users AS u', 'pa.id_user', '=', 'u.id')
			->leftJoin('users AS up', 'pa.activity_by', '=', 'up.id')
			->select('pa.id_activity', 'pa.id_user', 'u.id', 'u.name', 'u.email', 'pa.id_kuota_wawancara', 'pa.no_tiket', 'pa.id_workflow', 'pa.status', 'pa.id_template', 'pa.keterangan', 'pa.activity_at', 'pa.activity_by AS id_petugas', 'up.name AS nama_petugas', 'up.email AS email_petugas')
		  	->where('pa.no_tiket', '=', $no_tiket)
		  	->where('pa.id_workflow', '=', $id_workflow)
		  	->first();
		if($act)
		  return $act;
		else
		  return null;
	}

	public static function getBookingActivityCount($id_user)
	{
		$acts = DB::table('permohonan_activity')
			->where('id_user', '=', $id_user)
			->where('id_workflow', '=', 1)
			->where('no_tiket', '=', null)
			->count();
		if($acts)
			return $acts;
		else
			return null;
	}

	public static function createActivity($id_user, $id_kuota_wawancara, $no_tiket, $id_workflow, $status, $id_template, $keterangan)
	{
		$system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();
		$current_user = Auth::id();
		$act = DB::table('permohonan_activity')
				->insert(['id_user' => $id_user
						, 'id_kuota_wawancara' => $id_kuota_wawancara
						, 'no_tiket' =>  $no_tiket
						, 'id_workflow' => $id_workflow
						, 'status' => $status
						, 'id_template' => $id_template
						, 'keterangan' => $keterangan
						, 'activity_at' => $current_datetime
						, 'activity_by' => $current_user
						]);
	}

	public static function updateActivityNoTiket($id_activity, $no_tiket)
	{
		$act = DB::table('permohonan_activity')
            ->where('id_activity', '=', $id_activity)
            ->update([
                'no_tiket' => $no_tiket
            ]);
	}

	public static function updateActivity($id_activity, $status, $keterangan, $id_template)
	{
		$act = DB::table('permohonan_activity')
			->where('id_activity', '=', $id_activity)
			->update([
				'status' => $status,
				'keterangan' => $keterangan,
				'id_template' => $id_template
			]);
	}

	public static function deleteActivityNullTiketByUser($id_user, $id_workflow)
	{
		$delete_act = DB::table('permohonan_activity')
                    ->where('id_user', '=', $id_user)
                    ->where('id_workflow', '=', $id_workflow)
                    ->where('no_tiket', '=', null)
                    ->delete();
        if($delete_act)
        	return $delete_act;
        else
        	return null;
	}

	public static function getApplicantActivity($no_tiket)
	{
		$app = DB::table('permohonan as p')
			->leftJoin('permohonan_activity as pa', 'p.no_tiket', '=', 'pa.no_tiket')
			->leftJoin('ms_workflow as mw', 'pa.id_workflow', '=', 'mw.id_workflow')
			->leftJoin('biodata as b', 'pa.activity_by', '=', 'b.id_user')
			->select('mw.workflow', 'pa.status', 'pa.keterangan', 'pa.activity_at', 'b.nama')
			->where('p.no_tiket', '=', $no_tiket)
			->get();
      return $app;
	}
}