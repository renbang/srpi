<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\DB;
class Lokasi extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];


   //public $timestamps = false; 

   //whitelist
   //protected $fillable = ['title','description'];

   //blacklist
   //protected $guarded = ['id','created_at'];
  
   //klo ga mau ngelist whitelist, bikin guarded kosongan aja
   protected $guarded = [];

   public static function getProvinsi()
   {
       $listpropinsi = DB::table('ms_lokasi')->distinct()->select('provinsi')
         ->orderBy('kd_lokasi', 'asc')->get();

        return $listpropinsi;
   }

   public static function getKabupaten($provinsi)
   {
       $listkabupaten = DB::table('ms_lokasi')->distinct()->select('kabupaten')
         ->where('provinsi','=',$provinsi)
         ->orderBy('kd_lokasi', 'asc')->get();
        
        return $listkabupaten;
   }
   
   public static function getKecamatan($kabupaten)
   {
       $listkecamatan = DB::table('ms_lokasi')->distinct()->select('kecamatan')
         ->where('kabupaten','=',$kabupaten)
         ->orderBy('kd_lokasi', 'asc')->get();
        
        return $listkecamatan;
   }
   

   public static function getKelurahan($kecamatan)
   {
        $listkelurahan = DB::table('ms_lokasi')->distinct()->select('kd_lokasi','kelurahan','kodepos')
         ->where('kecamatan','=',$kecamatan)
         ->orderBy('kd_lokasi', 'asc')->get();
        
        return $listkelurahan;
   }
 
   public static function getCompleteLokasi($kd_lokasi)
   {
       $lokasi = DB::table('ms_lokasi')
          ->where('kd_lokasi','=',$kd_lokasi)
          ->first();
       if($lokasi)   
        return $lokasi;
      else return null;
   }


}
