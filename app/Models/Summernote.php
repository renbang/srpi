<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Summernote extends Model
{
    //

    public function up()
    {
        Schema::create('summernotes', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('content');
            $table->timestamps();
        });
    }
}
