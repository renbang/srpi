<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Lampiran extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public $table = 'ms_lampiran';
  
    protected $guarded = ['kd_lampiran'];

    public static function getMsLampiran()
    {
       $data = DB::table('ms_lampiran')->distinct()->select('kd_lampiran','id_lampiran','desc_lampiran')
         ->orderBy('kd_lampiran', 'asc')->get();

        return $data;
    }

    public static function getMasterLampiran()
    {
        $ms_lampiran = DB::table('ms_lampiran')->get();
        return $ms_lampiran;
    }

    public static function getApplicantAttachment($no_tiket)
    {
        $list_lampiran = DB::table('permohonan_file')
            ->select('id', 'nama_lampiran', 'size_lampiran', 'kd_lampiran', 'link')
            ->where('no_tiket', '=', $no_tiket)
            ->get();
        return $list_lampiran;
    }
}
