<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class KuotaWawancara extends Model
{
   use SoftDeletes;
   protected $dates = ['deleted_at'];


   //public $timestamps = false;
   public $table = 'v_kuota_wawancara';

   //whitelist
   //protected $fillable = ['title','description'];

   //blacklist
   //protected $guarded = ['id','created_at'];
  
   //klo ga mau ngelist whitelist, bikin guarded kosongan aja
   protected $guarded = ['id_kuota_wawancara'];

   public static function getAllTanggalWawancara()
   {
       $list_tanggal = DB::table('kuota_wawancara')
                           ->distinct()
                           ->select('id_kuota_wawancara', 'tgl_wawancara')
                           ->get();
        return $list_tanggal;
   }
}
