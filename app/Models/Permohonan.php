<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Permohonan extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
  public $table = 'permohonan';
  
  protected $guarded = ['no_tiket','created_at','updated_at'];
 
  public static function getActiveApplication($id_user)
  {
     
      $bio = DB::table('permohonan')
      ->where('id_user','=',$id_user)
      ->whereNotIn('id_workflow', [1, 2])
       ->orderBy('no_tiket', 'desc')
      ->first();
    if($bio)
      return $bio;
    else
      return null;
  }

  public static function getDataApplication($id_user)
  {
       $app = DB::table('biodata as a')
       ->leftJoin('permohonan as b', 'a.id_bio', '=', 'b.id_bio')
       ->leftJoin('ms_lokasi as c', 'a.kd_lokasi', '=', 'c.kd_lokasi')
       ->leftJoin('ms_workflow as d', 'b.id_workflow', '=', 'd.id_workflow')
       ->leftJoin('kuota_wawancara as e', 'b.id_kuota_wawancara', '=', 'e.id_kuota_wawancara')
       ->leftJoin('ms_jenjang as g', 'b.kd_jenjang', '=', 'g.kd_jenjang')
        ->select('a.nama', 'a.jns_kel', 'a.tmp_lahir', 'a.tgl_lahir', 'a.nik', 'a.no_paspor', 'a.alamat', 'c.provinsi', 'c.kabupaten', 'c.kecamatan', 'c.kelurahan', 'c.kodepos', 'a.email', 'a.no_tlp', 'b.no_tiket', 'b.id_workflow', 'd.workflow', 'b.id_kuota_wawancara', 'e.tgl_wawancara', 'b.alasan_kedatangan', 'g.desc_jenjang', 'b.nm_universitas', 'b.fakultas', 'b.jurusan')
        ->where('a.id_user','=',$id_user)
        ->whereNotIn('b.id_workflow', [1])
        ->orderBy('no_tiket', 'desc')
        ->first();
        if($app)
        return $app;
        else return null;
   }

  // public static function getDataApplication($id_user)
  // {
  //      $app = DB::table('biodata as a')
  //      ->leftJoin('permohonan as b', 'a.id_bio', '=', 'b.id_bio')
  //      ->leftJoin('ms_lokasi as c', 'a.kd_lokasi', '=', 'c.kd_lokasi')
  //      ->leftJoin('ms_workflow as d', 'b.id_workflow', '=', 'd.id_workflow')
  //      ->leftJoin('kuota_wawancara as e', 'b.id_kuota_wawancara', '=', 'e.id_kuota_wawancara')
  //      ->leftJoin('ms_jenjang as g', 'b.kd_jenjang', '=', 'g.kd_jenjang')
  //      ->leftJoin('permohonan_activity as h', function($join)
  //       {
  //           $join->on('h.no_tiket', '=', 'b.no_tiket')
  //                ->on('h.id_workflow', '=', 'b.id_workflow');
  //       })

  //       ->select('a.nama', 'a.jns_kel', 'a.tmp_lahir', 'a.tgl_lahir', 'a.nik', 'a.no_paspor', 'a.alamat', 'b.nm_universitas', 'b.fakultas', 'b.jurusan',
  //         'c.provinsi', 'c.kabupaten', 'c.kecamatan', 'c.kelurahan', 'c.kodepos', 'a.email', 'a.no_tlp', 'b.no_tiket', 'b.id_workflow', 'd.workflow',
  //         'd.id_template', 'd.message', 'b.id_kuota_wawancara', 'e.tgl_wawancara', 'b.alasan_kedatangan', 'g.desc_jenjang', 'h.keterangan')
  //       ->where('a.id_user','=',$id_user)
  //       ->whereNotIn('b.id_workflow', [1])
  //       ->orderBy('no_tiket', 'desc')
  //       ->first();
  //       if($app)
  //       return $app;
  //       else return null;
  //  }

   public static function getHistoryApplication($id_user)
   {
      DB::statement(DB::raw('set @row:=0'));
       $app = DB::table('permohonan as b')       
       ->leftJoin('ms_workflow as d', 'b.id_workflow', '=', 'd.id_workflow')
       ->leftJoin('kuota_wawancara as e', 'b.id_kuota_wawancara', '=', 'e.id_kuota_wawancara')
       ->leftJoin('ms_status_mahasiswa as f', 'b.kd_status_mahasiswa', '=', 'f.kd_status_mahasiswa')
       ->leftJoin('ms_jenjang as g', 'b.kd_jenjang', '=', 'g.kd_jenjang')
       ->selectRaw('b.no_tiket, b.id_workflow, d.workflow, b.id_kuota_wawancara, e.tgl_wawancara, b.alasan_kedatangan, b.tmp_kuliah, f.desc_status_mahasiswa,
          g.desc_jenjang, b.nm_universitas, b.thn_lulus, b.fakultas, b.jurusan, b.tgl_test, b.no_identitas_kandidat, b.skor, @row:=@row+1 as row')        
        ->where('b.id_user','=',$id_user)
        ->orderByRaw('no_tiket DESC')
        ->paginate(5);
        if($app)
        return $app;
        else return null;
   }

   public static function getCompleteDataApplication($id_user)
   {
      DB::statement(DB::raw('set @row:=0'));
       $app = DB::table('permohonan as b')       
       ->leftJoin('ms_workflow as d', 'b.id_workflow', '=', 'd.id_workflow')
       ->leftJoin('kuota_wawancara as e', 'b.id_kuota_wawancara', '=', 'e.id_kuota_wawancara')
       ->leftJoin('ms_status_mahasiswa as f', 'b.kd_status_mahasiswa', '=', 'f.kd_status_mahasiswa')
       ->leftJoin('ms_jenjang as g', 'b.kd_jenjang', '=', 'g.kd_jenjang')
       ->selectRaw('b.no_tiket, b.id_workflow, d.workflow, b.id_kuota_wawancara, e.tgl_wawancara, b.alasan_kedatangan, b.tmp_kuliah, f.desc_status_mahasiswa,
          g.desc_jenjang, b.nm_universitas, b.thn_lulus, b.fakultas, b.jurusan, b.tgl_test, b.no_identitas_kandidat, b.skor, @row:=@row+1 as row')        
        ->where('b.id_user','=',$id_user)
        ->whereNotIn('b.id_workflow', [1, 2])
        ->orderBy('no_tiket', 'desc')
        ->first();
        if($app)
        return $app;
        else return null;
   }

   public static function getApplicantDocumentList($tgl_wawancara)
   {
      $apps = DB::table(DB::raw('
      (
          SELECT pac.id_activity, pac.id_user, pac.id_kuota_wawancara, acts.no_tiket, acts.id_workflow AS id_wf, pac.status, pac.id_template, pac.keterangan, pac.activity_at, pac.activity_by
          FROM 
            (
              SELECT activities.no_tiket, MAX(activities.id_workflow) AS id_workflow
              FROM
                (
                  SELECT *
                  FROM permohonan_activity AS pa
                  ORDER BY pa.id_activity DESC
                ) AS activities
              GROUP BY activities.no_tiket
            ) AS acts
          INNER JOIN permohonan_activity AS pac
            ON pac.id_workflow = acts.id_workflow AND
               pac.no_tiket = acts.no_tiket
      ) AS recentActs'))
      ->rightJoin('permohonan AS p', 'recentActs.no_tiket', '=', 'p.no_tiket')
      ->leftJoin('biodata AS b', 'p.id_bio', '=', 'b.id_bio')
      ->leftJoin('ms_lokasi as mlok', 'b.kd_lokasi', '=', 'mlok.kd_lokasi')
      ->leftJoin('ms_workflow as mw', 'p.id_workflow', '=', 'mw.id_workflow')
      ->leftJoin('kuota_wawancara as kw', 'p.id_kuota_wawancara', '=', 'kw.id_kuota_wawancara')
      ->leftJoin('ms_jenjang as mj', 'p.kd_jenjang', '=', 'mj.kd_jenjang')
      ->leftJoin('ms_status_mahasiswa as msm', 'p.kd_status_mahasiswa', '=', 'msm.kd_status_mahasiswa')
      ->select('p.no_tiket', 'b.nama', 'b.jns_kel', 'b.nik', 'b.no_paspor', 'recentActs.status', 'kw.tgl_wawancara', 'p.id_workflow', 'recentActs.id_wf')
      ->where('kw.tgl_wawancara', '=', $tgl_wawancara)
      // ->where(function($apps)
      // {
      //     $apps->where('p.id_workflow', '=', 3)
      //          ->orWhere('p.id_workflow', '=', 4);
      // })
      // ->where(function($apps)
      // {
      //     $apps->where('recentActs.id_wf', '=', 3)
      //          ->orWhere('recentActs.id_wf', '=', 2);
      // })
      ->distinct()
      ->get();
      return $apps;
   }

   public static function getApplicantDocument($no_tiket)
   {
      $app = DB::table('biodata as b')
       ->leftJoin('permohonan as p', 'p.id_bio', '=', 'b.id_bio')
       ->leftJoin('kuota_wawancara as kw', 'p.id_kuota_wawancara', '=', 'kw.id_kuota_wawancara')
       ->leftJoin('ms_lokasi as mlok', 'b.kd_lokasi', '=', 'mlok.kd_lokasi')
       ->leftJoin('ms_workflow as mw', 'p.id_workflow', '=', 'mw.id_workflow')
       ->leftJoin('ms_jenjang as mj', 'p.kd_jenjang', '=', 'mj.kd_jenjang')
       ->leftJoin('ms_status_mahasiswa as msm', 'p.kd_status_mahasiswa', '=', 'msm.kd_status_mahasiswa')
       ->select('b.id_user', 'b.nama', 'b.jns_kel', 'b.tmp_lahir', 'b.tgl_lahir', 'b.nik', 'b.no_paspor', 'b.tgl_diterbitkan', 'b.tgl_hbs_berlaku', 'b.alamat', 'mlok.provinsi', 'mlok.kabupaten', 'mlok.kecamatan', 'mlok.kelurahan', 'mlok.kodepos', 'b.email', 'b.no_tlp', 'p.no_tiket', 'kw.id_kuota_wawancara', 'kw.tgl_wawancara', 'mj.desc_jenjang', 'msm.desc_status_mahasiswa', 'p.tmp_kuliah', 'p.thn_lulus', 'p.nm_universitas', 'p.alamat_universitas', 'p.tgl_test', 'p.no_identitas_kandidat', 'p.skor', 'p.alasan_kedatangan', 'p.fakultas', 'p.jurusan')
       ->where('p.no_tiket', '=', $no_tiket)
       ->first();
      return $app;
   }
   
   public static function getPresenceData($tgl_wawancara)
   {
      $apps = DB::table(DB::raw('
      (
          SELECT pac.id_activity, pac.id_user, pac.id_kuota_wawancara, acts.no_tiket, acts.id_workflow AS id_wf, pac.status, pac.id_template, pac.keterangan, pac.activity_at, pac.activity_by
          FROM 
            (
              SELECT activities.no_tiket, MAX(activities.id_workflow) AS id_workflow
              FROM
                (
                  SELECT *
                  FROM permohonan_activity AS pa
                  ORDER BY pa.id_activity DESC
                ) AS activities
              GROUP BY activities.no_tiket
            ) AS acts
          INNER JOIN permohonan_activity AS pac
            ON pac.id_workflow = acts.id_workflow AND
               pac.no_tiket = acts.no_tiket
      ) AS recentActs'))
      ->rightJoin('permohonan AS p', 'recentActs.no_tiket', '=', 'p.no_tiket')
      ->leftJoin('biodata AS b', 'p.id_bio', '=', 'b.id_bio')
      ->leftJoin('ms_lokasi as mlok', 'b.kd_lokasi', '=', 'mlok.kd_lokasi')
      ->leftJoin('ms_workflow as mw', 'p.id_workflow', '=', 'mw.id_workflow')
      ->leftJoin('kuota_wawancara as kw', 'p.id_kuota_wawancara', '=', 'kw.id_kuota_wawancara')
      ->leftJoin('ms_jenjang as mj', 'p.kd_jenjang', '=', 'mj.kd_jenjang')
      ->leftJoin('ms_status_mahasiswa as msm', 'p.kd_status_mahasiswa', '=', 'msm.kd_status_mahasiswa')
      ->leftJoin('permohonan_file as pf', 'p.no_tiket', '=', 'pf.no_tiket')
      ->leftJoin('ms_lampiran as mlam', 'pf.kd_lampiran', '=', 'mlam.kd_lampiran')
      ->leftJoin(DB::raw('
              (SELECT
                no_tiket AS no_tiket_interview,
                id_workflow AS id_wf_interview,
                activity_at AS activity_at_interview
              FROM permohonan_activity
              WHERE id_workflow = 4
              ) pa'), function($join)
              {
                $join->on('p.no_tiket', '=', 'pa.no_tiket_interview');
              })
      ->select('p.no_tiket', 'p.id_workflow', 'b.id_user', 'b.nama', 'b.jns_kel', 'b.nik', 'b.no_paspor', 'recentActs.status', 'recentActs.activity_at', 'b.tmp_lahir', 'b.tgl_lahir', 'kw.id_kuota_wawancara', 'kw.tgl_wawancara', 'mj.desc_jenjang', 'msm.desc_status_mahasiswa', 'p.fakultas', 'p.jurusan', 'p.nm_universitas', 'pf.link', 'p.id_workflow', 'recentActs.id_wf', 'pa.id_wf_interview', 'pa.activity_at_interview')
      ->where('kw.tgl_wawancara', '=', $tgl_wawancara)
      ->where('p.id_workflow', '>=', 4)
      // ->where(function($apps)
      // {
      //     $apps->where('p.id_workflow', '=', 4)
      //          ->orWhere('p.id_workflow', '=', 5);
      // })
      // ->where(function($apps)
      // {
      //     $apps->where('recentActs.id_wf', '=', 4)
      //          ->orWhere('recentActs.id_wf', '=', 3);
      // })
      ->where(function($apps)
      {
          $apps->where('mlam.kd_lampiran', '=', 1)
               ->orWhere('mlam.kd_lampiran', '=', null);
      })
      ->distinct()
      ->get();

      return $apps;
   }

   public static function getApplicantDataList($tgl_wawancara)
   {
      $apps = DB::table(DB::raw('
      (
          SELECT pac.id_activity, pac.id_user, pac.id_kuota_wawancara, acts.no_tiket, acts.id_workflow AS id_wf, pac.status, pac.id_template, pac.keterangan, pac.activity_at, pac.activity_by
          FROM 
            (
              SELECT activities.no_tiket, MAX(activities.id_workflow) AS id_workflow
              FROM
                (
                  SELECT *
                  FROM permohonan_activity AS pa
                  ORDER BY pa.id_activity DESC
                ) AS activities
              GROUP BY activities.no_tiket
            ) AS acts
          INNER JOIN permohonan_activity AS pac
            ON pac.id_workflow = acts.id_workflow AND
               pac.no_tiket = acts.no_tiket
      ) AS recentActs'))
      ->rightJoin('permohonan AS p', 'recentActs.no_tiket', '=', 'p.no_tiket')
      ->leftJoin('biodata AS b', 'p.id_bio', '=', 'b.id_bio')
      ->leftJoin('ms_lokasi as mlok', 'b.kd_lokasi', '=', 'mlok.kd_lokasi')
      ->leftJoin('ms_workflow as mw', 'p.id_workflow', '=', 'mw.id_workflow')
      ->leftJoin('kuota_wawancara as kw', 'p.id_kuota_wawancara', '=', 'kw.id_kuota_wawancara')
      ->leftJoin('ms_jenjang as mj', 'p.kd_jenjang', '=', 'mj.kd_jenjang')
      ->leftJoin('ms_status_mahasiswa as msm', 'p.kd_status_mahasiswa', '=', 'msm.kd_status_mahasiswa')
      ->select('p.no_tiket', 'p.id_workflow', 'b.nama', 'b.jns_kel', 'b.nik', 'b.no_paspor', 'recentActs.id_wf', 'recentActs.status', 'b.id_user', 'kw.id_kuota_wawancara')
      ->where('kw.tgl_wawancara', '=', $tgl_wawancara)
      ->where('p.id_workflow', '>=', 5)
      // ->where(function($apps)
      // {
      //     $apps->where('p.id_workflow', '=', 4)
      //          ->orWhere('p.id_workflow', '=', 5);
      // })
      // ->where(function($apps)
      // {
      //     $apps->where('recentActs.id_wf', '=', 4)
      //          ->orWhere('recentActs.id_wf', '=', 5);
      // })
      ->distinct()
      ->get();

      return $apps;
   }

   public static function getApplicantData($no_tiket)
   {
      $app = DB::table('biodata as b')
        ->leftJoin('permohonan as p', 'p.id_bio', '=', 'b.id_bio')
        ->leftJoin('kuota_wawancara as kw', 'p.id_kuota_wawancara', '=', 'kw.id_kuota_wawancara')
        ->leftJoin('ms_lokasi as mlok', 'b.kd_lokasi', '=', 'mlok.kd_lokasi')
        ->leftJoin('ms_workflow as mw', 'p.id_workflow', '=', 'mw.id_workflow')
        ->leftJoin('ms_jenjang as mj', 'p.kd_jenjang', '=', 'mj.kd_jenjang')
        ->leftJoin('ms_status_mahasiswa as msm', 'p.kd_status_mahasiswa', '=', 'msm.kd_status_mahasiswa')
        ->select('b.id_user', 'b.nama', 'b.jns_kel', 'b.tmp_lahir', 'b.tgl_lahir', 'b.nik', 'b.no_paspor', 'b.tgl_diterbitkan', 'b.tgl_hbs_berlaku', 'b.alamat', 'b.kd_lokasi', 'mlok.provinsi', 'mlok.kabupaten', 'mlok.kecamatan', 'mlok.kelurahan', 'mlok.kodepos', 'b.email', 'b.no_tlp', 'p.no_tiket', 'kw.id_kuota_wawancara', 'kw.tgl_wawancara', 'mj.kd_jenjang', 'mj.desc_jenjang', 'msm.kd_status_mahasiswa', 'msm.desc_status_mahasiswa', 'p.tmp_kuliah', 'p.thn_lulus', 'p.nm_universitas', 'p.alamat_universitas', 'p.tgl_test', 'p.no_identitas_kandidat', 'p.skor', 'p.alasan_kedatangan', 'p.fakultas', 'p.jurusan')
        //->where('kw.tgl_wawancara','=', '2018-05-09')
        ->where('p.no_tiket', '=', $no_tiket)
        ->first();
      return $app;
   }

   public static function updateApplicationStatus($no_tiket, $id_workflow)
   {
      $system_datetime = \Carbon\Carbon::now();
      $current_datetime  = $system_datetime->toDateTimeString();

      $res = DB::table('permohonan')
          ->where('no_tiket', '=', $no_tiket)
          ->update(['id_workflow' => $id_workflow,
                    'updated_at' => $current_datetime]);
      return $res;
   }
}


