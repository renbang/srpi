
<script src="vendor/jquery/jquery.min.js"></script>
<link href="/vendor/bootstrap/css/datepicker.css" rel="stylesheet">


@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

<style>
    .sticky {
      position: fixed;
      top: 80;
      left: 0;
      right: 0;
      width: 100%;
      padding-top: 10px;
      z-index: 1000;
      text-align: center;
      -webkit-transition: 0.3s;
      -moz-transition: 0.3s;
      transition: 0.3s;
      background-color: white;
      box-shadow: 0 12px 12px -12px rgba(0, 0, 0, 0.5);
    }
</style>

<script type="text/javascript">
    window.onscroll = function() {myFunction()};

    function myFunction() {
        var navbar = document.getElementById("container-wizard");
        var sticky = navbar.offsetTop;
        if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }

    var _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf"];
    function fileValidation(e) {
      oInput = e;

      if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
          var blnValid = false;
          for (var j = 0; j < _validFileExtensions.length; j++) {
            var sCurExtension = _validFileExtensions[j];
            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                blnValid = true;
                break;
            }
          }
           
          if (!blnValid) {
            alert("Format file tidak valid, yang diperbolehkan hanya: " + _validFileExtensions.join(", "));
            oInput.value = "";
            return false;
          }
        }
      }

      if(oInput.files[0].size > 1100000) {
        alert("Ukuran lampiran tidak boleh melebihi 1 MB");
        oInput.value = "";
        return false;
      }

      return true;
    }
    // var uploadField = document.getElementsByClassName("input-lampiran");

    // uploadField.onchange = function() {
    //     if(this.files[0].size > 2200000){
    //        alert("File is too big!");
    //        this.value = "";
    //     };
    //     alert(this.files[0].size);
    // };

</script>

@section('content')

<script type="text/javascript">
$(document).ready(function(){
    
    $('#refreshBtn').click(function() {  
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
}); 
</script>
<?php 
$nama='';
?>
<center>
    <div class="container-cus" id="container-wizard">
        <div class="row">
            <div class="wizard">
                <div class="col-sm-1 col-xs-1">
                    
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/1.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">RESER&shy;VASI&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/2_sel.png') }}" class="wizard-item-selected-icon">
                    <div class="wizard-item-selected-text"><strong>ISI FORM &emsp; <i class="fa fa-clock-o"></i> <i id="timer"></i></strong></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/3.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">VERIFI&shy;KASI</div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/4.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">WAWAN&shy;CARA</div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/5.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">PENER&shy;BITAN</div>
                </div>
                <div class="col-sm-1 col-xs-1">
                    
                </div>
            </div>
        </div>
    </div>
    <hr class="line-shadow">
</center>

<div class="container">   
  <form action="/savePermohonan" method="post" enctype="multipart/form-data" id="form-addPermohonan"> 
  <div class="col-md-12">    
    <div class="row">
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #c1fff3;"><strong>A.&emsp;TANGGAL WAWANCARA</strong></div>
            <div class="panel-body">
              <h5>{{ \Carbon\Carbon::parse($tanggal)->format('d F Y') }}</h5>
              <!-- ( {{ $id_kuota_wawancara }} ) -->
              <input type="hidden" name="tgl_wawancara" value={{ $tanggal }}>
              <input type="hidden" name="id_kuota_wawancara" value={{ $id_kuota_wawancara }}>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #c1fff3;"><strong>B.&emsp;BIODATA</strong></div>
            <div class="panel-body">
              <div class="form-group row">
                <div class="col-sm-5">Nama</div>
                <div class="col-sm-7">
                  <input type="text" name="nama" class="form-control input-sm alphaOnly" id="nama" value="{{ ($biodata)?$biodata->nama:'' }}" maxlength="50" style="text-transform:uppercase" required="required">
                    @if($errors->has('nama'))
                      <p>{{ $errors->first('nama') }}</p>
                    @endif
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Jenis Kelamin</div>
                <div class="col-sm-7">      
                  <?php $Lstat = "checked";
                        $Pstat = "";
                        if($biodata) { 
                          if($biodata->jns_kel == 'P') {
                            $Lstat = "";
                            $Pstat = "checked";
                          }                         
                        }
                  ?>
                  <input type="radio" id="jns_kel" name="jns_kel" value="L" {{ $Lstat }}>&nbsp;Laki-Laki</input> &emsp;
                  <input type="radio" id="jns_kel" name="jns_kel" value="P" {{ $Pstat }} >&nbsp;Perempuan</input>
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tempat Lahir</div>
                <div class="col-sm-7">
                  <input type="text" name="tmp_lahir" class="form-control input-sm alphaOnly" id="tmp_lahir" value="{{ ($biodata)?$biodata->tmp_lahir:'' }}" style="text-transform:uppercase" required="required">
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tanggal Lahir</div>
                <div class="col-sm-7">
                  <div class="form-group">
                    <div class="input-group date">
                      <?php 
                        $tgl_lahir_ddmmyyyy = '';
                        if($biodata){
                            $tgl_lahir_yyyyMMdd=$biodata->tgl_lahir;
                            $tgl_lahir_ddmmyyyy= date('d-m-Y', strtotime($tgl_lahir_yyyyMMdd));
                        } 
                      ?>
                      <input type="text" class="form-control input-sm datepicker" name="tgl_lahir" id="tgl_lahir" value="{{ $tgl_lahir_ddmmyyyy }}" placeholder="dd-mm-yyyy" style="padding-left: 10px" required="required">
                      <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </div>
                     </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">NIK</div>
                <div class="col-sm-7">
                  <input type="text" name="nik" class="form-control input-sm numberOnly" maxlength="16" id="nik" value="{{ ($biodata)?$biodata->nik:'' }}" style="text-transform:uppercase" required="required">
                </div>
              </div>
              <hr>
              <div class="form-group row">    
                <div class="col-sm-5">Nomor Paspor</div>
                <div class="col-sm-7">
                  <input type="text" name="no_paspor" class="form-control input-sm" maxlength="8" id="no_paspor" value="{{ ($biodata)?$biodata->no_paspor:'' }}" style="text-transform:uppercase" required="required">
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tanggal Diberikan Paspor</div>
                <div class="col-sm-7">
                  <div class="form-group">
                    <div class="input-group date">
                      <?php 
                        $tgl_diterbitkan_ddmmyyyy = '';
                        if($biodata){
                            $tgl_diterbitkan_yyyyMMdd=$biodata->tgl_diterbitkan;
                            $tgl_diterbitkan_ddmmyyyy= date('d-m-Y', strtotime($tgl_diterbitkan_yyyyMMdd));
                        } 
                      ?>
                      <input type="text" class="form-control input-sm datepicker" name="tgl_diterbitkan" id="tgl_diterbitkan" value="{{ $tgl_diterbitkan_ddmmyyyy }}" style="padding-left: 10px" required="required">
                      <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </div>
                     </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tanggal Habis Berlaku Paspor</div>
                <div class="col-sm-7">
                  <div class="form-group">
                    <div class="input-group date">
                      <?php 
                        $tgl_hbs_berlaku_ddmmyyyy = '';
                        if($biodata){
                            $tgl_hbs_berlaku_yyyyMMdd=$biodata->tgl_hbs_berlaku;
                            $tgl_hbs_berlaku_ddmmyyyy= date('d-m-Y', strtotime($tgl_hbs_berlaku_yyyyMMdd));
                        } 
                      ?>
                      <input type="text" class="form-control input-sm datepicker" name="tgl_hbs_berlaku" id="tgl_hbs_berlaku" value="{{ $tgl_hbs_berlaku_ddmmyyyy }}" style="padding-left: 10px" required="required">
                      <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </div>
                     </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="form-group row">    
                <div class="col-sm-5">Alamat Lengkap</div>
                <div class="col-sm-7">
                  <textarea name="alamat" class="form-control input-sm" rows="5" id="alamat" style="text-transform:uppercase" required="required">{{ ($biodata)?$biodata->alamat:'' }}</textarea>
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Provinsi</div>
                <div class="col-sm-7"> 
                  <select  class="form-control input-sm" name="provinsi" id="provinsi">  
                    <option value="0"  selected="true">Pilih Provinsi</option>  
                    @foreach ($listpropinsi as $propinsi)
                      <option value="{{ $propinsi->provinsi }}">{{ $propinsi->provinsi }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Kota/Kabupaten</div>
                <div class="col-sm-7">   
                  <select class="form-control input-sm" name="kabupaten" id="kabupaten">                    
                    <option value="0" disable="true" selected="true">Pilih Kota/Kabupaten</option>
                    @if($lokasi)
                      @foreach ($listkabupaten as $kabupaten)
                        <option value="{{ $kabupaten->kabupaten }}">{{ $kabupaten->kabupaten }}</option>
                      @endforeach
                    @endif
                  </select>                    
               </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Kecamatan</div>
                <div class="col-sm-7">
                  <select class="form-control input-sm" name="kecamatan" id="kecamatan">
                    <option value="0" disable="true" selected="true">Pilih Kecamatan</option>
                    @if($lokasi)
                      @foreach ($listkecamatan as $kecamatan)
                        <option value="{{ $kecamatan->kecamatan }}">{{ $kecamatan->kecamatan }}</option>
                      @endforeach
                    @endif
                  </select>     
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Kelurahan/Desa</div>
                <div class="col-sm-7">
                  <select class="form-control input-sm" name="kelurahan" id="kelurahan">
                    <option value="0" disable="true" selected="true">Pilih Kelurahan</option>
                    @if($lokasi)
                      @foreach ($listkelurahan as $kelurahan)
                        <option value="{{ $kelurahan->kd_lokasi }}">{{ $kelurahan->kelurahan }}</option>
                      @endforeach
                    @endif
                  </select> 
                </div>
              </div>
              <hr>
              <div class="form-group row">    
                <div class="col-sm-5">Email</div>
                <div class="col-sm-7">
                  <input type="email" name="email" class="form-control input-sm" id="email" value="{{ ($biodata)?$biodata->email:'' }}" required="required">
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Nomor Handphone / Telepon</div>
                <div class="col-sm-7">
                  <input type="text" name="no_tlp" class="form-control input-sm numberOnly" id="no_tlp" value="{{ ($biodata)?$biodata->no_tlp:'' }}" style="text-transform:uppercase" required="required">
                </div>
              </div>
            </div>
          </div>
        </div> <!--end of left side-->
        
        <div class="col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #c1fff3;"><strong>C.&emsp;PENDIDIKAN</strong></div>
            <div class="panel-body">
              <div class="form-group row">    
                <div class="col-sm-5">Tempat Kuliah</div>
                <div class="col-sm-7">
                  <?php $DNstat = "checked";
                  $LNstat = "";
                  // if($biodata->tmp_kuliah) { 
                  //   if($biodata->tmp_kuliah == 'Dalam Negeri') {
                  //     $DNstat = "checked";
                  //     $LNstat = "";
                  //   }                         
                  // }
                ?>
                <label class="radio-inline">
                  <input type="radio" id="tmp_kuliah" name="tmp_kuliah" value="Dalam Negeri" {{ $DNstat }}>Dalam Negeri
                </label>
                <label class="radio-inline">
                  <input type="radio" id="tmp_kuliah" name="tmp_kuliah" value="Luar Negeri" {{ $LNstat }}>Luar Negeri
                </label>
               </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Status Mahasiswa</div>
                <div class="col-sm-7">
                  <select  class="form-control input-sm" id="kd_status_mahasiswa" name="kd_status_mahasiswa">  
                    <option value="0"  selected="true">Pilih Status</option>   
                    @foreach ($liststatusmhs as $statusmhs)
                      <option value="{{ $statusmhs->kd_status_mahasiswa }}">{{ $statusmhs->desc_status_mahasiswa }}</option>
                    @endforeach
                  </select>
               </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Jenjang</div>
                <div class="col-sm-7">
                   <select  class="form-control input-sm" id="kd_jenjang" name="kd_jenjang">  
                      <option value="0"  selected="true">Pilih Jenjang</option>   
                      @foreach ($listjenjang as $jenjang)
                        <option value="{{ $jenjang->kd_jenjang }}">{{ $jenjang->desc_jenjang }}</option>
                      @endforeach
                    </select>
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Perguruan Tinggi</div>
                <div class="col-sm-7">
                  <input type="text" name="nm_universitas" class="form-control input-sm" maxlength="100" id="nm_universitas" value="" style="text-transform:uppercase" required="required">
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Fakultas</div>
                <div class="col-sm-7">
                  <input type="text" name="fakultas" class="form-control input-sm" maxlength="150" id="fakultas" value="" style="text-transform:uppercase" required="required">
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Jurusan</div>
                <div class="col-sm-7">
                  <input type="text" name="jurusan" class="form-control input-sm" maxlength="150" id="jurusan" value="" style="text-transform:uppercase" required="required">
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Lulus Tahun</div>
                <div class="col-sm-7">
                  <input type="text" name="thn_lulus" class="form-control input-sm numberOnly" maxLength="4" id="thn_lulus" value="" style="text-transform:uppercase" required="required">
                </div>
              </div>
              
              <div class="form-group row">    
                <div class="col-sm-5">Alamat Perguruan Tinggi</div>
                <div class="col-sm-7">
                  <textarea name="alamat_universitas" class="form-control input-sm" rows="5" id="alamat_universitas" style="text-transform:uppercase" required="required"></textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #c1fff3;"><strong>D.&emsp;IELTS / TOEFL iBT</strong></div>
            <div class="panel-body">
              <div class="form-group row">    
                <div class="col-sm-5">Nilai/Skor</div>
                <div class="col-sm-7">
                  <input type="text" name="skor" class="form-control input-sm numberOnly" id="skor" value="" style="text-transform:uppercase" required="required">
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tanggal Test</div>
                <div class="col-sm-7">
                  <div class="form-group">
                    <div class="input-group date">
                      <input type="text" class="form-control input-sm datepicker" name="tgl_test" id="tgl_test" value="" style="padding-left: 10px" required="required">
                      <div class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </div>
                     </div>
                  </div>
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">No. Identitas Kandidat</div>
                <div class="col-sm-7">
                  <input type="text" name="no_identitas_kandidat" class="form-control input-sm" id="no_identitas_kandidat" value="" style="text-transform:uppercase" required="required">
                </div>
              </div>
            </div>            
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #c1fff3;"><strong>E.&emsp;TUJUAN KEDATANGAN SELAMA DI AUSTRALIA</strong></div>
            <div class="panel-body">
              <div class="form-group row">    
                <div class="col-sm-5">Deskripsi</div>
                <div class="col-sm-7">
                  <textarea name="alasan_kedatangan" class="form-control input-sm" rows="10" id="alasan_kedatangan" required="required"></textarea>
                </div>
              </div>
            </div>
          </div>                     
        </div><!--end of right side-->

       
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #c1fff3;"><strong>F.&emsp;UNGGAH DOKUMEN</strong></div>
            <div class="panel-body">
              <div class="col-sm-6">
                <?php $no=0; 
                   for($no = 0;$no<count($listlampiran)/2;$no++){
                   ?>                  
                    <div class="row form-input" style="padding-bottom: 10px">
                      <div class="col-sm-4">{{ $no+1 }} .&emsp; <i>{{ $listlampiran[$no]->desc_lampiran }}</i></div>
                      <div class="col-sm-7"><input type="file" name="{{ $listlampiran[$no]->id_lampiran }}" id="{{ $listlampiran[$no]->id_lampiran }}" class="form-control input-sm input-lampiran" onchange="fileValidation(this);" accept=".jpeg,.jpg,.png,.pdf"/></div>
                    </div>
                <?php } ?>  
              </div> 
              <div class="col-sm-6">
                <?php $no2 = $no;
                   for($no2 = $no;$no2<count($listlampiran);$no2++){
                   ?>
                    <div class="row form-input" style="padding-bottom: 10px">
                      <div class="col-sm-5">{{ $no2+1 }} .&emsp; <i>{{ $listlampiran[$no2]->desc_lampiran }}</i></div>
                      <div class="col-sm-7"><input type="file" name="{{ $listlampiran[$no2]->id_lampiran }}" id="{{ $listlampiran[$no2]->id_lampiran }}" class="form-control input-sm input-lampiran" onchange="fileValidation(this);" accept=".jpeg,.jpg,.png,.pdf"/></div>
                    </div>
                <?php } ?>  
              </div> 
            </div>
          </div>
        </div>
          <div  class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <div class="col-sm-6">
                   <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }} row">
                      <div class="col-md-12" align="left">  
                        <div class="captcha">                        
                            <span>{!! captcha_img() !!}</span>
                            <button type="button" id="refreshBtn" class="btn btn-success"><i class="fa fa-refresh"></i></button>
                        </div>
                                   
                          <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">

                          @if ($errors->has('captcha'))
                              <span class="help-block">
                                  <strong>Wrong Captcha</strong>
                              </span>
                          @endif
                      </div>

                </div>
                </div>
                <div class="col-sm-6">
                  <div class="col-sm-1"><input id="eula" type="checkbox" /></div>
                  <div class="col-sm-11">Ya, semua data yang saya isi adalah benar.</div>
                </div>
              </div>
            </div>
            <div class="form-group pull-right">
              <input type="hidden" name="id_activity" value="{{ $id_activity }}">   
              <input type="submit" class="btn btn-lg btn-success" id="submit" name="submit" value="C R E A T E">
            </div>
          </div>
        {{ csrf_field() }} 
    </div>
  </div>  
  </form>
</div>    
<div id="formTimerOver" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body">
        <p>Waktu pengisan form Anda sudah habis!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>            
@endsection

<script type="text/javascript">  
$(document).ready(function(){
  $('#submit').hide();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

  $(".numberOnly").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
  });


  $('.alphaOnly').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str) || e.which == 8 || e.which == 32 || e.which == 46 || e.which == 16 || e.which == 20) {
        return true;
    }

    e.preventDefault();
    return false;
  });

}); 



$(function(){
    
    $('#provinsi').change(function(e){
        var provinsi = e.target.value;
        console.log(provinsi);

        if(provinsi){

          $.ajax({
             type:"GET",
             url:"{{url('api/get-kabupaten-list')}}?provinsi="+provinsi,
             success:function(res){  
              if(res){

                  $("#kabupaten").empty();
                  $("#kabupaten").append('<option value="0" disable="true" selected="true">Pilih Kota/Kabupaten</option>');

                  $('#kecamatan').empty();
                  $('#kecamatan').append('<option value="0" disable="true" selected="true">Pilih Kecamatan</option>');

                  $('#kelurahan').empty();
                  $('#kelurahan').append('<option value="0" disable="true" selected="true">Pilih Kelurahan</option>');


                  $.each(res,function(index,Obj){
                      $("#kabupaten").append('<option value="'+Obj.kabupaten+'">'+Obj.kabupaten+'</option>');
                  });
             
              }else{
                 $("#kabupaten").empty();
                 $("#kecamatan").empty();
                 $("#kelurahan").empty();

              }
             }
          });
      }else{
          $("#kabupaten").empty();
      }      

    });


    $('#kabupaten').change(function(e){
        var kabupaten = e.target.value;
        console.log(kabupaten);

        if(kabupaten){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kecamatan-list')}}?kabupaten="+kabupaten,
             success:function(res){              
              if(res){                  

                  $('#kecamatan').empty();
                  $('#kecamatan').append('<option value="0" disable="true" selected="true">Pilih Kecamatan</option>');

                  $.each(res,function(index,Obj){
                      $("#kecamatan").append('<option value="'+Obj.kecamatan+'">'+Obj.kecamatan+'</option>');
                  });
             
              }else{
                 $("#kecamatan").empty();
              }
             }
          });
      }else{
          $("#kecamatan").empty();
      }      

    });

    $('#kecamatan').change(function(e){
        var kecamatan = e.target.value;
        console.log(kecamatan);

        if(kecamatan){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kelurahan-list')}}?kecamatan="+kecamatan,
             success:function(res){               
              if(res){                  

                  $('#kelurahan').empty();
                  $('#kelurahan').append('<option value="0" disable="true" selected="true">Pilih Kelurahan</option>');

                  $.each(res,function(index,Obj){
                      $("#kelurahan").append('<option value="'+Obj.kd_lokasi+'">'+Obj.kelurahan+'</option>');
                  });
             
              }else{
                 $("#kelurahan").empty();
              }
             }
          });
        }
        else{
          $("#kelurahan").empty();
        }      
    });
});

$(function(){
    var totalSec = 900; //900 for 15 minutes
    var timer = setInterval(function() {
      var min = "0" + Math.floor(totalSec / 60);
      var sec = "0" + (totalSec - min * 60);
      var count = 0;
      var intervalId = setInterval(function() {  
        if ($('#timer').css('visibility') == 'hidden') {
          $('#timer').css('visibility', 'visible');
          $('#timer').text(min.substr(-2) + ":" + sec.substr(-2));
          if (count++ === 1) {
            clearInterval(intervalId);
          }
        } else {
          $('#timer').css('visibility', 'hidden');
        }    
      }, 75);
      totalSec--;
      if (totalSec == 60) {
        $('#timer').addClass("font-color-red")
      }
      if (totalSec == -1)
      {
        clearInterval(timer);
        $('#formTimerOver').modal('show');
        setTimeout(function() { window.location.href = '/home';}, 1500);
      } 
    }, 1000);
 }); 

$(function(){
   $('#eula').change(function(e) {
    if ( $(this).is(':checked') ) $('#submit').show();
    else {
      alert("Anda belum menyetujui kebenaran data yang anda isi");
      $('#submit').hide();
    }
  });
 }); 

$(function(){
  $.fn.datepicker.defaults.autoclose = true;
  $(".datepicker").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    todayHighlight: true
  });

});

$(function(){  
    <?php if( $lokasi ){ ?>
      $('[name=provinsi]').val('{{ $lokasi->provinsi }}');
      $('[name=kabupaten]').val('{{ $lokasi->kabupaten }}');
      $('[name=kecamatan]').val('{{ $lokasi->kecamatan }}');
      $('[name=kelurahan]').val('{{ $lokasi->kd_lokasi }}');
    <?php } ?>
});

</script>
