<script src="/vendor/jquery/jquery.min.js"></script>

<style>
    .sticky {
      position: fixed;
      top: 80;
      left: 0;
      right: 0;
      width: 100%;
      padding-top: 10px;
      z-index: 1000;
      text-align: center;
      -webkit-transition: 0.3s;
      -moz-transition: 0.3s;
      transition: 0.3s;
      background-color: white;
      box-shadow: 0 12px 12px -12px rgba(0, 0, 0, 0.5);
    }
</style>

<script type="text/javascript">
    window.onscroll = function() {myFunction()};

    function myFunction() {
        var navbar = document.getElementById("container-wizard");
        var sticky = navbar.offsetTop;
        if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }
</script>


@extends('layouts.master')
 <meta name="csrf-token" content="<?php echo csrf_token() ?>">
@section('content')

<center>
    <div class="container-cus" id="container-wizard">
        <div class="row">
            <div class="wizard">
                <div class="col-sm-1 col-xs-1">
                    
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/1.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">RESER&shy;VASI&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/2.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">ISI FORM&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/3.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">VERIFI&shy;KASI&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/4_sel.png') }}" class="wizard-item-selected-icon">
                    <div class="wizard-item-selected-text"><strong>WAWAN&shy;CARA&emsp;</strong></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/5.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">PENER&shy;BITAN</div>
                </div>
                <div class="col-sm-1 col-xs-1">
                    
                </div>
            </div>
        </div>
    </div>
    <hr class="line-shadow">
</center>


<div class="container">   
	<div class="panel panel-default">
        <div class="panel-heading"><strong>INTERVIEW</strong></div>
        <div class="panel-body" >
            <div class="col-sm-12">
              @if($flag == 1 || $flag == 99 || $flag == -99)
                <table class="table borderless">
                    <tr>
                        <td><h4><small>Tanggal Wawancara</small></h4></td>
                        <td><h5><strong>{{ \Carbon\Carbon::parse($activity->activity_at)->addHours(7)->format('d F Y') }}</strong></h5></td>
                    </tr>
                    <tr>
                        <td><h4><small>Pukul</small></h4></td>
                        <td><h5><strong>{{ \Carbon\Carbon::parse($activity->activity_at)->addHours(7)->format('H:i:s') }} WIB</strong></h5></td>
                    </tr>
                    <tr>
                        <td><h4><small>Petugas Check-In</small></h4></td>
                        <td><h5><strong>{{ $activity->nama_petugas }}</strong></h5></td>
                    </tr>
                    @if(!empty($activity_wawancara) > 0)
                      <tr>
                        <td><h4><small>Petugas Wawancara</small></h4></td>
                        <td><h5><strong>{{ ($activity_wawancara->nama_petugas)?$activity_wawancara->nama_petugas:'-' }}</strong></h5></td>
                      </tr>
                    @endif
                </table>
              @elseif($flag == 0)
                <br>
                <form action="/downloadPDF" method="post" style="margin-bottom: 0px;">           
                  <input type="submit" class="btn btn-success btn-lg" name="submit" value="Download Interview Invitation Letter">
                  {{ csrf_field() }}
                  <input type="hidden" name="id_workflow" value="3">
                  <input type="hidden" name="_method" value="POST">
                </form>
                <hr>
                <table class="table borderless">
                  <tr>
                    <td><h4><small>Tanggal Wawancara</small></h4></td>
                    <td><h5><strong>{{ \Carbon\Carbon::parse($application->tgl_wawancara)->format('d F Y') }}</strong></h5></td>
                  </tr>
                </table>
              @elseif($flag == -1)
                <hr>
                <table class="table borderless">
                  <tr>
                    <td><h4><small>Tanggal</small></h4></td>
                    <td><h5><strong>{{ \Carbon\Carbon::parse($application->tgl_wawancara)->format('d F Y') }}</strong></h5></td>
                  </tr>
                  <tr>
                    <td><h4><small>Pukul</small></h4></td>
                    <td><h5><strong><i>TIDAK HADIR</i></strong></h5></td>
                  </tr>
                  <tr>
                    <td><h4><small>Petugas Check-In</small></h4></td>
                    <td><h5><strong>-</strong></h5></td>
                  </tr>
                  <tr>
                    <td><h4><small>Petugas Wawancara</small></h4></td>
                    <td><h5><strong>-</strong></h5></td>
                  </tr>
                </table>
              @endif
            </div>           
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading"><strong>STATUS</strong></div>
        <div class="panel-body" >
            <div class="col-sm-12">
                <p>{{ $message }}</p>
                <br>
                @if($flag == 99)
                    <form action="/downloadPDF" method="post" style="margin-bottom: 0px;">           
                        <input type="submit" class="btn btn-success btn-lg" name="submit" value="Download Recommendation Letter (E-SRPI)">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_workflow" value="6">
                        <input type="hidden" name="_method" value="POST">
                    </form>

                @elseif($flag == -99)
                    <form action="/downloadPDF" method="post" style="margin-bottom: 0px;">           
                        <input type="submit" class="btn btn-danger btn-lg" name="submit" value="Download Rejection Letter">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_workflow" value="5">
                        <input type="hidden" name="_method" value="POST">
                    </form>

              @endif
            </div>           
        </div>
    </div>
</div>        	
@endsection

