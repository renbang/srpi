<script src="/vendor/jquery/jquery.min.js"></script>
<style>
    .sticky {
      position: fixed;
      top: 80;
      left: 0;
      right: 0;
      width: 100%;
      padding-top: 10px;
      z-index: 1000;
      text-align: center;
      -webkit-transition: 0.3s;
      -moz-transition: 0.3s;
      transition: 0.3s;
      background-color: white;
      box-shadow: 0 12px 12px -12px rgba(0, 0, 0, 0.5);
    }
</style>

<script type="text/javascript">
    window.onscroll = function() {myFunction()};

    function myFunction() {
        var navbar = document.getElementById("container-wizard");
        var sticky = navbar.offsetTop;
        if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }
</script>

@extends('layouts.master')
 <meta name="csrf-token" content="<?php echo csrf_token() ?>">
@section('content')

<center>
    <div class="container-cus" id="container-wizard">
        <div class="row">
            <div class="wizard">
                <div class="col-sm-1 col-xs-1">
                    
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/1.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">RESER&shy;VASI&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/2.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">ISI FORM&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/3_sel.png') }}" class="wizard-item-selected-icon">
                    <div class="wizard-item-selected-text"><strong>VERIFI&shy;KASI</strong></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/4.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">WAWAN&shy;CARA&emsp;</div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/5.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">PENER&shy;BITAN</div>
                </div>
                <div class="col-sm-1 col-xs-1">
                    
                </div>
            </div>
        </div>
    </div>
    <hr class="line-shadow">
</center>


<div class="container">   
  @if($biodata)
  <div class="panel panel-default">
    <div class="panel-heading"><strong>APPLICATION</strong></div>
    <div class="panel-body" >

        <div class="col-sm-12">
          <p>{{ $message }}</p>
          <br>
          <center>
            <a href="{{action('PermohonanController@downloadSummaryPDF')}}" class="btn btn-lg btn-default" >Download Application Summary</a>
          </center>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
              <div class="form-group row">
                <div class="col-sm-5">Nama</div>
                <div class="col-sm-7">{{ ($biodata)?$biodata->nama:'' }}</div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Jenis Kelamin</div>
                <div class="col-sm-7">{{ ($biodata->jns_kel == 'P')?'PEREMPUAN':'LAKI-LAKI' }}                      
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tempat/Tanggal Lahir</div>
                <div class="col-sm-7">{{ ($biodata)?$biodata->tmp_lahir:'' }},  {{ \Carbon\Carbon::parse($biodata->tgl_lahir)->format('d F Y') }}</div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">NIK</div>
                <div class="col-sm-7">{{ ($biodata)?$biodata->nik:'' }}</div>
              </div>
            </div>
            <div class="col-sm-6">                        

              <div class="form-group row">    
                <div class="col-sm-5">Nomor Paspor</div>
                <div class="col-sm-7">{{ ($biodata)?$biodata->no_paspor:'' }}</div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tanggal Diberikan Paspor</div>
                <div class="col-sm-7">{{ \Carbon\Carbon::parse($biodata->tgl_diterbitkan)->format('d F Y') }}</div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tanggal Habis Berlaku Paspor</div>                        
                <div class="col-sm-7">{{ \Carbon\Carbon::parse($biodata->tgl_hbs_berlaku)->format('d F Y') }}</div>                          
              </div> 
            </div>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
              <div class="form-group row">    
                <div class="col-sm-5">Alamat Lengkap</div>
                <div class="col-sm-7">{{ ($biodata)?$biodata->alamat:'' }}</div>
              </div>  
              <div class="form-group row">    
                <div class="col-sm-5"></div>
                <div class="col-sm-7">{{ ($lokasi)?$lokasi->provinsi:'' }}
                <br>
                {{ ($lokasi)?$lokasi->kabupaten:'' }}
                <br>
                {{ ($lokasi)?$lokasi->kecamatan:'' }}
                <br>
                {{ ($lokasi)?$lokasi->kelurahan:'' }}
                <br>
                {{ ($lokasi)?$lokasi->kodepos:'' }}
                </div>
              </div>  
             
            </div>
            <div class="col-sm-6">
              <div class="form-group row">    
                <div class="col-sm-5">Email</div>
                <div class="col-sm-7">{{ ($biodata)?$biodata->email:'' }}</div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Nomor Handphone / Telepon</div>
                <div class="col-sm-7">{{ ($biodata)?$biodata->no_tlp:'' }}</div>
              </div>  
            </div>
        </div>                     
        
        <div class="col-sm-12">
            <hr>
        </div>

        <div class="col-sm-12">
            <div class="col-sm-6">
              <div class="form-group row">
                <div class="col-sm-5">Nomor Permohonan</div>
                <div class="col-sm-7">{{ ($activeapp)?$activeapp->no_tiket:'' }}</div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tanggal Wawancara</div>
                <div class="col-sm-7">{{ \Carbon\Carbon::parse($activeapp->tgl_wawancara)->format('d F Y') }}                    
                </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Tujuan Kedatangan</div>
                <div class="col-sm-7">{{ $activeapp->alasan_kedatangan }}</div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">IELTS / TOEFL</div>
                <div class="col-sm-7">{{ $activeapp->skor }}</div>
              </div> 
               <div class="form-group row">    
                <div class="col-sm-5">Tanggal Tes</div>
                <div class="col-sm-7">{{ \Carbon\Carbon::parse($activeapp->tgl_test)->format('d F Y') }}</div>
              </div>
               <div class="form-group row">    
                <div class="col-sm-5">No Identitas Kandidat</div>
                <div class="col-sm-7">{{ $activeapp->no_identitas_kandidat }}</div>
              </div>

            </div>
            <div class="col-sm-6">                        
              <div class="form-group row">    
                <div class="col-sm-5">Riwayat Pendidikan Tinggi</div>
                <div class="col-sm-7"></div>
              </div>      
              <div class="form-group row">    
                <div class="col-sm-5">Tempat Kuliah</div>
                <div class="col-sm-7">{{ ($activeapp)?$activeapp->tmp_kuliah:'' }}</div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Status Mahasiswa</div>
                <div class="col-sm-7">{{ $activeapp->desc_status_mahasiswa }} </div>
              </div>
              <div class="form-group row">    
                <div class="col-sm-5">Jenjang</div>                        
                <div class="col-sm-7">{{ $activeapp->desc_jenjang }}</div>
              </div> 
              <div class="form-group row">    
                <div class="col-sm-5">Nama Universitas</div>                        
                <div class="col-sm-7">{{ $activeapp->nm_universitas }}</div>
              </div> 
              <div class="form-group row">    
                <div class="col-sm-5">Tahun Lulus</div>                        
                <div class="col-sm-7">{{ $activeapp->thn_lulus }}</div>
              </div> 
            </div>
        </div>             
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading"><strong>STATUS</strong></div>
    <div class="panel-body" >
      @if(!empty($activity) > 0)
        @if($activity->status == 'Y')
          <h5>CONGRATULATIONS, YOUR DOCUMENT IS PASSED THE VERIFICATION PROCESS. PLEASE DOWNLOAD THE INTERVIEW INVITATION LETTER.</h4>
          

        @elseif($activity->status == 'N')
          <h4><small>YOUR DOCUMENT IS REJECTED.</small></h4>
 
        @endif

        <br>
        <form action="/downloadPDF" method="post" style="margin-bottom: 0px;">           
          <input type="submit" class="btn btn-success btn-lg" name="submit" value="Download Interview Invitation Letter">
          {{ csrf_field() }}
          <input type="hidden" name="id_workflow" value="3">
          <input type="hidden" name="_method" value="POST">
        </form>
      
      @else
        <h4><small>VERIFICATION IS BEING ON PROCESS.</small></h4>     

      @endif

    </div>
  </div>
  @else
    <p>Hai, sistem kami belum mempunyai data diri Anda.</p>
  @endif

  <br>
    
</div>        	
@endsection

