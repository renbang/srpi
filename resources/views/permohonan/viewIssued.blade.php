<script src="/vendor/jquery/jquery.min.js"></script>
<style>
    .sticky {
      position: fixed;
      top: 80;
      left: 0;
      right: 0;
      width: 100%;
      padding-top: 10px;
      z-index: 1000;
      text-align: center;
      -webkit-transition: 0.3s;
      -moz-transition: 0.3s;
      transition: 0.3s;
      background-color: white;
      box-shadow: 0 12px 12px -12px rgba(0, 0, 0, 0.5);
    }
</style>

<script type="text/javascript">
    window.onscroll = function() {myFunction()};

    function myFunction() {
        var navbar = document.getElementById("container-wizard");
        var sticky = navbar.offsetTop;
        if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }
</script>

@extends('layouts.master')
 <meta name="csrf-token" content="<?php echo csrf_token() ?>">
@section('content')

<center>
    <div class="container-cus" id="container-wizard">
        <div class="row">
            <div class="wizard">
                <div class="col-sm-1 col-xs-1">
                    
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/1.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">RESER&shy;VASI&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/2.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">ISI FORM&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/3.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">VERIFI&shy;KASI&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/4.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text-done">WAWAN&shy;CARA&emsp;<i class="fa fa-check-circle"></i></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/5_sel.png') }}" class="wizard-item-selected-icon">
                    <div class="wizard-item-selected-text"><strong>PENER&shy;BITAN</strong></div>
                </div>
                <div class="col-sm-1 col-xs-1">
                    
                </div>
            </div>
        </div>
    </div>
    <hr class="line-shadow">
</center>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><strong>E-SRPI</strong></div>
        <div class="panel-body" >
            <center>
                <div class="col-sm-12">
                    <p>{{ $message }}</p>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12">
                    <form action="/downloadPDF" method="post" style="margin-bottom: 0px;">           
                        <input type="submit" class="btn btn-success btn-lg" name="submit" value="Download Recommendation Letter (E-SRPI)">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_workflow" value="6">
                        <input type="hidden" name="_method" value="POST">
                    </form>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12">
                    <table class="table borderless">
                        <tr>
                            <td><h4><small>Review Wawancara</small></h4></td>
                            <td><h5><strong>{{ $activity->keterangan }}</strong></h5></td>
                        </tr>
                    </table>
                </div>
            </center>
        </div>
    </div>
</div>        	
@endsection

