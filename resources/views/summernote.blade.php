 
<!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
 
<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>    
 
<script>
$(document).ready(function() {

    //initialize summernote
    $('.summernote').summernote(
        {height: 300,
            width: 1500}
        );

   

});
</script>
 

 <form action="{{route('summernotePersist')}}" method="POST">
        {{ csrf_field() }}
        <textarea name="summernoteInput" class="summernote"></textarea>
        <br>
        <button type="submit">Submit</button>
    </form>


