<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SRPI DITJEN IMIGRASI</title>


  <!-- J S -->

    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap-datepicker.js"></script>
    <script src="/js/jqBootstrapValidation.js"></script>
    <script src="/js/contact_me.js"></script>
    <script src="/js/freelancer.min.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>

  <!-- C S S -->

    <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendor/bootstrap/css/datepicker.css" rel="stylesheet">
    <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="/css/freelancer.css" rel="stylesheet">
    <link href="/css/site.css" rel="stylesheet">
</head>

<body id="page-top">
  <div class="navigation-bar">
    <nav class="navbar navbar-default navbar-inverse navbar-expand-lg navbar-dark bg-dark fixed-top navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapsible">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-header-section">
            <a class="navbar-brand" href="#">
              <img src="{{ URL::to('img/imigrasi-small.png') }}" alt="Dispute Bills">
              <small><i class="navbar-brand-web-title">Direktorat Jenderal Imigrasi</i><br><strong>SURAT REKOMENDASI PEMERINTAH INDONESIA</strong></small>
            </a>

          </div>
        </div>
        <div class="navbar-collapse collapse" id="navbar-collapsible">
          <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
                @guest
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle freelancer-color a-nav-item" href="{{ route('login') }}" role="button"><i class="fa fa-sign-in i-nav-item"></i><br>{{ __('Login') }}</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle freelancer-color a-nav-item" href="{{ route('register') }}" role="button"><i class="fa fa-id-badge i-nav-item"></i><br>{{ __('Register') }}</a>
                  </li>
                @else
                 <li class="nav-item dropdown">                   
                    <a class="nav-link dropdown-toggle freelancer-color a-nav-item" href="{{ route('home') }}" role="button"><i class="fa fa-home i-nav-item"></i><br>{{ __(' Home ') }}</a>                    
                 </li>
                 <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle freelancer-color a-nav-item" href="{{ route('riwayat') }}" role="button" ><i class="fa fa-list i-nav-item"></i><br>{{ __('History') }}</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle freelancer-color a-nav-item" href="{{ route('current') }}" role="button"  id="a-nav-item-current"><i class="fa fa-map-marker i-nav-item" id="i-nav-item-current"></i><br>{{ __('Current') }}</a>
                  </li>
                  @if ( Auth::user()->role != "User") 
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle freelancer-color a-nav-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fa fa-paper-plane i-nav-item"></i><br>
                          {{ __('Workflow') }} <span class="caret"></span>
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <ul class="nav dropdown-list">
                            <li>
                              <a class="nav-link dropdown-toggle  freelancer-color" href="{{ route('verdok') }}" style="padding:15px 10px;">
                                  {{ __('1. Verifikasi Dokumen') }}
                              </a>
                            </li>
                            <li class="page-scroll">
                              <a class="nav-link dropdown-toggle  freelancer-color" href="{{ route('kehadiran') }}" style="padding:15px 10px;">
                                  {{ __('2. Kehadiran Wawancara') }}
                              </a>
                            </li>
                            <li>
                              <a class="nav-link dropdown-toggle  freelancer-color" href="{{ route('verdata') }}" style="padding:15px 10px;">
                                  {{ __('3. Review Data') }}
                              </a>
                            </li>
                          </ul>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
                  @endif
                  @if ( Auth::user()->role == "Administrator") 
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle freelancer-color a-nav-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fa fa-cog i-nav-item"></i><br>
                          {{ __('Setting') }} <span class="caret"></span>
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <ul class="nav dropdown-list">
                            <li class="page-scroll">
                              <a class="nav-link dropdown-toggle  freelancer-color" href="{{ route('templatePenolakanVerdok') }}" style="padding:15px 10px;">
                                  {{ __('Template Surat') }}
                              </a>
                            </li>
                            <li>
                              <a class="nav-link dropdown-toggle  freelancer-color" href="{{ route('signature') }}" style="padding:15px 10px;">
                                  {{ __('Tanda Tangan') }}
                              </a>
                            </li>
                            <li>
                              <a class="nav-link dropdown-toggle  freelancer-color" href="{{ route('kuota') }}" style="padding:15px 10px;">
                                  {{ __('Kuota Wawancara') }}
                              </a>
                            </li>
                          </ul>
                      </div>
                  </li>
                  @endif                
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle  freelancer-color a-nav-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fa fa-user i-nav-item"></i><br>
                          {{ Auth::user()->name }} <span class="caret"></span>
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <ul class="nav dropdown-list">
                            <li>
                              <a href="{{ route('logout') }}"
                                 onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="padding:15px 10px;">
                                  {{ __('Log Out') }}
                              </a>
                            </li>
                          </ul>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
                @endguest
            
          </ul>
        </div>
        <!--/.nav-collapse -->
      </div>
      <!--/.container-fluid -->
    </nav>
  </div>

	<div style="height: 90px"></div>

	@yield('content')

<!-- Footer -->
  <footer class="text-center footer-fixed-below">
      <div class="footer-above">
          <div class="container">
              <div class="row">
                  
              </div>
          </div>
      </div>
      <div class="footer-below">
          <div class="container">
            <small>&copy; <script>document.write(new Date().getFullYear())</script> - Direktorat Sistem dan Teknologi Informasi Keimigrasian</small>
            <br>
            Direktorat Jenderal Imigrasi
          </div>
        </div>
      </div>
  </footer>

  <script type="text/javascript">
    var url = window.location;
    
    if (url.pathname == "/viewverification" || url.pathname == "/viewinterview" || url.pathname == "/viewissued") {
      $('.nav-item #a-nav-item-current').parent().addClass('active');
    } else {
      $('.nav-item a[href="'+ url +'"]').parent().addClass('active');
    }

    $('.nav-item a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');

  </script>

</body>
</html>
