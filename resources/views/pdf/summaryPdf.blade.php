<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body> 

 


<div class="container">     
  <div class="panel panel-default">   
    <div class="panel-body" >
        <div class="col-sm-12">
            <table>
              <tr>
                <th>Nama</th>                        
                <td>{{ ($biodata)?$biodata->nama:'' }}</td>
                <th>Nomor Paspor</th>                        
                <td>{{ ($biodata)?$biodata->no_paspor:'' }}</td>
              </tr>
              <tr>
                <th>Jenis Kelamin</th>                        
                <td>{{ ($biodata->jns_kel == 'P')?'PEREMPUAN':'LAKI-LAKI' }}    </td>
                <th>Tanggal Diberikan Paspor</th>                        
                <td>{{ \Carbon\Carbon::parse($biodata->tgl_diterbitkan)->format('d F Y') }}</td>
              </tr>

              <tr>
                <th>Tempat/Tanggal Lahir</th>                        
                <td>{{ ($biodata)?$biodata->tmp_lahir:'' }},  {{ \Carbon\Carbon::parse($biodata->tgl_lahir)->format('d F Y') }}</td>
                <th>Tanggal Habis Berlaku Paspor</th>                        
                <td>{{ \Carbon\Carbon::parse($biodata->tgl_hbs_berlaku)->format('d F Y') }}</td>
              </tr>
              <tr>
                <th>NIK</th>                        
                <td>{{ ($biodata)?$biodata->nik:'' }}</td>
                <th></th>                        
                <td></td>
              </tr>

              <tr>
                <td colspan="4"><hr></td>
              </tr>

              <tr>
                <th>Alamat Lengkap</th>                        
                <td>{{ ($biodata)?$biodata->alamat:'' }}</td>
                <th>Email</th>                        
                <td>{{ ($biodata)?$biodata->email:'' }}</td>
              </tr>
              <tr>
                <th></th>                        
                <td>{{ ($lokasi)?$lokasi->provinsi:'' }}</td>
                <th>Nomor Handphone / Telpon</th>                        
                <td>{{ ($biodata)?$biodata->no_tlp:'' }}</td>
              </tr>
              <tr>
                <th></th>                        
                <td>{{ ($lokasi)?$lokasi->kabupaten:'' }}
                    <br>
                    {{ ($lokasi)?$lokasi->kecamatan:'' }}
                    <br>
                    {{ ($lokasi)?$lokasi->kelurahan:'' }}
                    <br>
                    {{ ($lokasi)?$lokasi->kodepos:'' }}</td>
                <th></th>                        
                <td></td>
              </tr>

              <tr>
                <td colspan="4"><hr></td>
              </tr>
              
              <tr>
                <th>Nomor Permohonan</th>                        
                <td>{{ ($activeapp)?$activeapp->no_tiket:'' }}</td>
                <th>Riwayat Pendidikan Tinggi</th>                        
                <td></td>
              </tr>
              <tr>
                <th>Tanggal Wawancara</th>                        
                <td>{{ \Carbon\Carbon::parse($activeapp->tgl_wawancara)->format('d F Y') }}  </td>
                <th>Tempat Kuliah</th>                        
                <td>{{ ($activeapp)?$activeapp->tmp_kuliah:'' }}</td>
              </tr>
              <tr>
                <th>Tujuan Kedatangan</th>                        
                <td>{{ $activeapp->alasan_kedatangan }}  </td>
                <th>Status Mahasiswa</th>                        
                <td>{{ $activeapp->desc_status_mahasiswa }}</td>
              </tr>
              <tr>
                <th>IELTS / TOEFL</th>                        
                <td>{{ $activeapp->skor }}</td>
                <th>Jenjang</th>                        
                <td>{{ $activeapp->desc_jenjang }}</td>
              </tr>
              <tr>
                <th>Tanggal Tes</th>                        
                <td>{{ \Carbon\Carbon::parse($activeapp->tgl_test)->format('d F Y') }}</td>
                <th>Nama Universitas</th>                        
                <td>{{ $activeapp->nm_universitas }}</td>
              </tr>
              <tr>
                <th>No Identitas Kandidat</th>                        
                <td>{{ $activeapp->no_identitas_kandidat }}</td>
                <th>Tahun Lulus</th>                        
                <td>{{ $activeapp->thn_lulus }}</td>
              </tr>
            </table> 
        </div>
    </div>
  </div>
</div>          
 

  </body>
</html>