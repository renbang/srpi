<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
  <head>
  		<style>
            /** Define the margins of your page **/
            /*@page {
                margin: 100px 25px;
            }*/

            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 2cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 2cm;

                /** Extra personal styles **/
                background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 1.5cm;
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2cm;

                /** Extra personal styles **/
                background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 1.5cm;
            }

            p.a {    
			    font-family: Arial;
			    font-size: 15px;

			}

        </style>

    <meta charset="utf-8">
    <title></title>
  </head>
  <body> 

<div class="container">
	<div class="col-sm-12">
    	<div class="row" align="center">
    		<?php $headerwidth = 200;
    		if($template->header == 'img/kumham.png') $headerwidth = 75;
    		else $headerwidth = 200; ?>
			<img style="width: {{$headerwidth}}px;" src="{{ $template->header }}" alt="Header" >
		</div>
  	</div>
  	<div class="col-sm-12">
    	<div class="row">
			<p class="a">{!! $template->template !!}</p>
		</div>
  	</div>
  	
  	<div class="col-sm-12">
		<table class="table table-bordered">
			<tbody>
			<tr>
				<td style="width: 500px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td style="width: 100px; text-align: center; font-size: 14px">{{ $ttd->jabatan }}</td>
			</tr>
			<tr>
				<td style="width: 500px"></td>
				<td style="width: 100px"></td>
			</tr>
			<tr>
				<td style="width: 500px"></td>
				<td style="width: 100px"></td>
			</tr>
			<tr>
				<td style="width: 500px"></td>
				<td style="width: 100px; text-align: center;"><img style="width: 100px;" src="{{ public_path(str_replace('public','/storage',$ttd->link_signature)) }}" alt="Signature"></td>
			</tr>
			<tr>
				<td style="width: 500px"></td>
				<td style="width: 100px; text-align: center; font-size: 14px">{{ $ttd->nama_pejabat }}</td>
			</tr>
			</tbody>
		</table>
  	</div>
 <br>
  	<div class="col-sm-12">
    	<div class="row">
			<p class="a">{!! $template->template_footer !!}</p>
		</div>
  	</div>

</div>

    
  </body>
</html>