<script src="/vendor/jquery/jquery.min.js"></script>

<style>
  .table-data-history {
    background-color: #d9edf7;
    text-align: center;
    vertical-align: middle;
    padding: 5px;
    border: 1px solid #c7deea;
  }
</style>

@extends('layouts.master')
 <meta name="csrf-token" content="<?php echo csrf_token() ?>">
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
          @if($biodata)
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #c1fff3;"><strong>Hai,&emsp;{{ ($biodata)?$biodata->nama:'' }} ! </strong></div>
            <div class="panel-body" >
                <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group row">
                        <div class="col-sm-5">Nama</div>
                        <div class="col-sm-7">{{ ($biodata)?$biodata->nama:'' }}</div>
                      </div>
                      <div class="form-group row">    
                        <div class="col-sm-5">Jenis Kelamin</div>
                        <div class="col-sm-7">{{ ($biodata->jns_kel == 'P')?'PEREMPUAN':'LAKI-LAKI' }}                      
                        </div>
                      </div>
                      <div class="form-group row">    
                        <div class="col-sm-5">Tempat/Tanggal Lahir</div>
                        <div class="col-sm-7">{{ ($biodata)?$biodata->tmp_lahir:'' }},  {{ \Carbon\Carbon::parse($biodata->tgl_lahir)->format('d F Y') }}</div>
                      </div>
                      <div class="form-group row">    
                        <div class="col-sm-5">NIK</div>
                        <div class="col-sm-7">{{ ($biodata)?$biodata->nik:'' }}</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">    
                        <div class="col-sm-5">Nomor Paspor</div>
                        <div class="col-sm-7">{{ ($biodata)?$biodata->no_paspor:'' }}</div>
                      </div>
                      <div class="form-group row">    
                        <div class="col-sm-5">Tanggal Diberikan Paspor</div>
                        <div class="col-sm-7">{{ \Carbon\Carbon::parse($biodata->tgl_diterbitkan)->format('d F Y') }}</div>
                      </div>
                      <div class="form-group row">    
                        <div class="col-sm-5">Tanggal Habis Berlaku Paspor</div>                        
                        <div class="col-sm-7">{{ \Carbon\Carbon::parse($biodata->tgl_hbs_berlaku)->format('d F Y') }}</div>
                      </div> 
                    </div>
                </div>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group row">    
                        <div class="col-sm-5">Alamat Lengkap</div>
                        <div class="col-sm-7">{{ ($biodata)?$biodata->alamat:'' }}</div>
                      </div>  
                      <div class="form-group row">    
                        <div class="col-sm-5"></div>
                        <div class="col-sm-7">{{ ($lokasi)?$lokasi->provinsi:'' }}
                        <br>
                        {{ ($lokasi)?$lokasi->kabupaten:'' }}
                        <br>
                        {{ ($lokasi)?$lokasi->kecamatan:'' }}
                        <br>
                        {{ ($lokasi)?$lokasi->kelurahan:'' }}
                        <br>
                        {{ ($lokasi)?$lokasi->kodepos:'' }}
                        </div>
                      </div>  
                     
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">    
                        <div class="col-sm-5">Email</div>
                        <div class="col-sm-7">{{ ($biodata)?$biodata->email:'' }}</div>
                      </div>
                      <div class="form-group row">    
                        <div class="col-sm-5">Nomor Handphone / Telepon</div>
                        <div class="col-sm-7">{{ ($biodata)?$biodata->no_tlp:'' }}</div>
                      </div>  
                    </div>
                </div>                     
                                 
                <div class="col-sm-12">
                  <table class="table table-condensed table-hover table-history">
                    <thead>
                        <tr>
                            <th rowspan="2" class="table-data-history"> Tiket </th>
                            <th rowspan="2" class="table-data-history"> Tanggal Wawancara  </th>
                            <th rowspan="2" class="table-data-history"> Tujuan Kedatangan </th>
                            <th colspan="5" class="table-data-history"> Riwayat Pendidikan Tinggi</th>
                            <th colspan="3" class="table-data-history"> IELTS / TOEFL </th>
                            <th rowspan="2" class="table-data-history"> Status Permohonan </th>
                        </tr>
                        <tr style="">
                            <th class="table-data-history"> Tempat Kuliah </th>
                            <th class="table-data-history"> Status Mahasiswa </th>
                            <th class="table-data-history"> Jenjang  </th>
                            <th class="table-data-history"> Nama Universitas </th>
                            <th class="table-data-history"> Tahun Lulus</th>
                            <th class="table-data-history"> Tanggal Tes </th>
                            <th class="table-data-history"> No Identitas Kandidat </th>
                            <th class="table-data-history"> Skor </th>
                        </tr>

                    </thead>
                    <tbody> <?php $no=1; ?>
                         @foreach($application as $key => $value)
                          <tr>
                              <td> #{{ $value->no_tiket }} </td>
                              <td> {{ \Carbon\Carbon::parse($value->tgl_wawancara)->format('d F Y') }} </td>
                              <td> {{ $value->alasan_kedatangan }} </td>
                              <td> {{ $value->tmp_kuliah }} </td>
                              <td> {{ $value->desc_status_mahasiswa }} </td>
                              <td> {{ $value->desc_jenjang }} </td>
                              <td> {{ $value->nm_universitas }} </td>
                              <td> {{ $value->thn_lulus }} </td>
                              <td> {{ \Carbon\Carbon::parse($value->tgl_test)->format('d F Y') }} </td>
                              <td> {{ $value->no_identitas_kandidat }} </td>
                              <td> {{ $value->skor }} </td>
                              <td> {{ $value->workflow }} </td>
                          </tr>
                         @endforeach
                    </tbody>
                  </table>
                  <div style="text-align: center;">
                    {{ $application->links() }}
                  </div>
                </div>

              
            </div>
          </div>

          @else
            <p>Hai, sistem kami belum mempunyai data diri Anda.</p>
          @endif
        </div> 
    </div>
</div>      
@endsection

