@extends('layouts.master')
<style>
    h2 {
       width: 100%; 
       text-align: center; 
       border-bottom: 1px solid #000; 
       line-height: 0.1em;
       margin: 10px 0 20px; 
    } 

    h2 span { 
        background:#fff; 
        padding:0 10px; 
    }
</style>


@section('content')
<script type="text/javascript">
$(document).ready(function(){
    
    $('#refreshBtn').click(function() {  
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
}); 
</script>
<center>
    <h4>Log in to SRPI System</h4>
    <hr>
    <div class="container">
        <div class="col-md-4 col-sm-2">

        </div>
        <div class="col-md-4 col-sm-8 col-xs-12">
            @if (!empty($user_message))
                <div class="alert alert-warning">
                  <strong>{{ $user_message }}</strong>
                </div>
            @endif
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Alamat Email" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Kata Sandi" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }} row">
                      <div class="col-md-12" align="left">  
                      <div class="captcha">                        
                          <span>{!! captcha_img() !!}</span>
                          <button type="button" id="refreshBtn" class="btn btn-success"><i class="fa fa-refresh"></i></button>
                      </div>

                        </div>
                        <div class="col-md-12">                                    
                          <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha" required>

                          @if ($errors->has('captcha'))
                              <span class="help-block">
                                  <strong>Wrong Captcha</strong>
                              </span>
                          @endif
                      </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="form form-inline">
                            <div class="pull-left">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Ingat Saya') }}
                                    </label>
                                </div>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Lupa Kata Sandi?') }}    
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">                                
                        <button type="submit" class="btn btn-primary btn-login">
                            {{ __('L O G I N') }}
                        </button>                                
                    </div>
                </div>
            </form>
                <div class="form-group row">
                    <div class="col-md-12">
                        <h6 class="auth-alt"><span class="auth-alt-text">atau </span></h6>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <a href="{{ url('/auth/google') }}">
                            <button type="submit" class="btn btm-primary btn-login-google">
                                <i class="fa fa-google-plus"></i>&emsp;Log In with <strong>Google</strong>
                            </button>
                        </a>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <center>
                        <div class="col-md-12">
                           <span> {{ _('Belum punya akun?') }} &emsp; <strong><a href="{{ route('register') }}">{{ _('Daftar Sekarang!') }}</a></strong></span>
                        </div>
                    </center>
                </div>
            
        </div>
        <div class="col-md-4 col-sm-2">

        </div>
    </div>
</center>
@endsection