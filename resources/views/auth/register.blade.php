@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')



<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
}); 
</script>
<center>
    <h4>Register to SRPI System</h4>
    <hr>
    <div class="container">
        <div class="col-md-4 col-sm-2">

        </div>
        <div class="col-md-4 col-sm-8 col-xs-12">
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Nama" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Alamat Email" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">                            
                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Kata Sandi" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Ulangi Kata Sandi" required>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }} row">
                      <div class="col-md-12" align="left">  
                      <div class="captcha">                        
                          <span>{!! captcha_img() !!}</span>
                          <button type="button" id="refreshBtn" class="btn btn-success"><i class="fa fa-refresh"></i></button>
                      </div>

                        </div>
                        <div class="col-md-12">                                    
                          <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha" required>

                          @if ($errors->has('captcha'))
                              <span class="help-block">
                                  <strong>Wrong Captcha</strong>
                              </span>
                          @endif
                      </div>

                </div>

                <hr>
                <div class="form-group row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-login">
                            {{ __('Daftar') }}
                        </button>
                    </div>
                </div>

        {{ csrf_field() }} 
            </form>
        </div>
        <div class="col-md-4 col-sm-2">

        </div>
    </div>
</center>
@endsection


