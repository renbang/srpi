<script src="/vendor/jquery/jquery.min.js"></script>

@extends('layouts.master')
 <meta name="csrf-token" content="<?php echo csrf_token() ?>">
@section('content')


<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>    



<div class="container">       
    <div class="col-sm-12">  
        
    <ul class="nav nav-tabs">
      <li class="active"><a href="#">PENOLAKAN VERIFIKASI DOKUMEN</a></li>
      <li><a href="{{ route('templatePenolakanVerdata') }}">PENOLAKAN VERIFIKASI DATA</a></li>
      <li><a href="{{ route('templateUndanganWawancara') }}">UNDANGAN WAWANCARA</a></li>
      <li><a href="{{ route('templateSuratRekomendasi') }}">REKOMENDASI PEMERINTAHAN INDONESIA</a></li>
    </ul>
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color: #c1fff3;"><strong>SETTING TEMPLATE SURAT PENOLAKAN VERIFIKASI DOKUMEN</strong></div>
        <div class="panel-body">
        
        <form action="/saveTemplate" method="post" enctype="multipart/form-data">    

        <div class="form-group row">
            <div class="col-sm-2">Header</div>
            <div class="col-sm-10">   
                <?php $kumhamimistat = "checked";
                    $kumhamstat = "";
                    if($summernote) { 
                      if($summernote->header == 'img/kumham.png') {
                        $kumhamimistat = "";
                        $kumhamstat = "checked";
                      }                         
                    }
                ?>   
                <input type="radio" id="header" name="header" value="img/kumhamimi.png" {{ $kumhamimistat }} > &emsp;<img src="{{ URL::to('img/kumhamimi.png') }}" height="100"></input> &emsp;
                <input type="radio" id="header" name="header" value="img/kumham.png"  {{ $kumhamstat }}> &emsp;<img src="{{ URL::to('img/kumham.png') }}" height="100"></input>
            </div>
        </div>

        

        <div class="form-group row">
            <div class="col-sm-12"><textarea name="template" id="template" class="summernote"></textarea>
            </div>
        </div>



        <div class="form-group row">
            <div class="col-sm-2">TTD</div>
            <div class="col-sm-10">   
                
               <select  class="form-control input-sm" name="signature" id="signature">  
                    <option value="0"  selected="true">Pilih Pejabat Penandatangan</option>  
                    @foreach ($listsign as $sign)
                      <option value="{{ $sign->id }}">{{ $sign->jabatan }} | {{ $sign->nama_pejabat }}</option>
                    @endforeach
                  </select>
                
            </div>
        </div>
       

        <div class="form-group row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10"> 
                <img style="width: 100px;" src=" {{ str_replace('public','/storage',$ttd->link_signature) }}" alt="Signature">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12"><textarea name="template_footer" id="template_footer" class="summernote_footer"></textarea>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
<b>PANDUAN : </b>
<br><i>Gunakan tag berikut untuk parameter template surat 
<br>Nama Pendaftar :<span style="color: red">&lt;nama&gt;</span> 
<br>Nomor Permohonan  :  <span style="color: red">&lt;no_tiket&gt;</span> 
<br>Jenis Kelamin : <span style="color: red">&lt;jns_kel&gt;</span> 
<br>Tempat / Tanggal Lahir : <span style="color: red">&lt;tmp_lahir&gt;</span> , <span style="color: red">&lt;tgl_lahir&gt;</span> 
<br>NIK : <span style="color: red">&lt;nik&gt;</span> 
<br>Nomor Paspor : <span style="color: red">&lt;no_paspor&gt;</span> 
<br>Alamat : <span style="color: red">&lt;alamat&gt;</span> , <span style="color: red">&lt;kelurahan&gt;</span> , <span style="color: red">&lt;kecamatan&gt;</span> , <span style="color: red">&lt;kabupaten&gt;</span> , <span style="color: red">&lt;provinsi&gt;</span> , <span style="color: red">&lt;kodepos&gt;</span> 
<br>Pendidikan : <span style="color: red">&lt;desc_jenjang&gt;</span> 
<br>Nama Universitas : <span style="color: red">&lt;nm_universitas&gt;</span> 
<br>Alasan Penolakan : <span style="color: red">&lt;keterangan&gt;</span> 
</i>
            </div>
        </div>

        <br>

              <input type="hidden" name="template_nama" value={{ $summernote->template_nama }}>
              <input type="hidden" name="template_id" value={{ $summernote->id }}>
        <div class="form-group row">
            <div class="col-sm-12" align="center">
                <button type="submit" class="btn btn-lg btn-success">Submit</button>
                <a href="{{ route('contohPDF', 1) }}" class="btn btn-lg btn-info" >Contoh PDF</a>
            </div>
        </div>
        {{ csrf_field() }}


        </form>
        </div>
    </div>
    </div>
</div>          
@endsection

<script>
$(document).ready(function() {

    //initialize summernote
    $('.summernote').summernote({
            height: 300,
            width: 1075,
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
            ],
        });

    //initialize summernote footer
    $('.summernote_footer').summernote({
            height: 100,
            width: 1075,
            toolbar: [
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
            ],
        });


    $('#template').summernote('code', {!! json_encode($summernote->template) !!});
    $('#template_footer').summernote('code', {!! json_encode($summernote->template_footer) !!});
 
});


$(function(){  
    <?php if( $summernote ){ ?>
      $('[name=signature]').val('{{ $summernote->signature }}');
    <?php } ?>
});


</script>