<script src="/vendor/jquery/jquery.min.js"></script>

@extends('layouts.master')
 <meta name="csrf-token" content="<?php echo csrf_token() ?>">
@section('content')




<div class="container">
    <div class="row">
    	
        <div class="col-sm-12">
          
          @if($listsign)
          <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #c1fff3;">Setting Master Tanda Tangan</div>
            <div class="panel-body" >
        		<div class="col-sm-12">
			    	<div class="container">
					   <form action="/signature_add" method="post" enctype="multipart/form-data"> 
					    <div class="form-group row">
					      <label for="lgFormGroupInput" class="col-sm-2 col-form-label col-form-label-lg">Jabatan</label>
					      <div class="col-sm-7">
					        <input type="text" name="jabatan" class="form-control input-sm alphaOnly" id="jabatan" value="{{ ($sign)?$sign->jabatan:'' }}">

              				<input type="hidden" name="id_sign" value={{ ($sign)?$sign->id:'' }}>
					      </div>
					    </div>
					    <div class="form-group row">
					      <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Nama Pejabat</label>
					      <div class="col-sm-7">
					        <input type="text" name="nama_pejabat" class="form-control input-sm alphaOnly" id="nama_pejabat" value="{{ ($sign)?$sign->nama_pejabat:'' }}">
					      </div>
					    </div>

					    <div class="form-group row">
					      <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Tanda Tangan</label>
                @if($sign)
                <div class="col-sm-10">
                  <img style="width: 100px;" src=" {{ str_replace('public','/storage',$sign->link_signature) }}" ald="Signature">
                </div>
                <div class="col-sm-2"></div>
                @endif
					      
                <div class="col-sm-7">
					        <input type="file" name="signature" id="signature" class="form-control input-sm" /></textarea>
					      </div>
					    </div>
					    <div class="form-group row">
					      <div class="col-sm-2"></div>
					      <div class="col-sm-7">
            				<input type="submit" class="btn btn-sm btn-success" name="submit" value="Create">
            				</div>
					    </div>

        				{{ csrf_field() }} 
					  </form>
					</div>

			    </div>


                <div class="col-sm-12">
                    <table class="table table-hover" >
                    <thead>
                        <tr  style="text-align-last: center; ">
                            <th > Jabatan  </th>
                            <th > Nama Pejabat </th>
                            <th > Tanda Tangan</th>
                            <th > Edit</th>
                            <th > Delete</th>
                        </tr>
                       

                    </thead>
                    <tbody> <?php $no=1; ?>
                         @foreach($listsign as $key => $value)
                          <tr style="text-align-last: center; ">
                              <td> {{ $value->jabatan }} </td>
                              <td> {{ $value->nama_pejabat }} </td>
                              <td> {{ str_replace('public','/storage',$value->link_signature) }} <img style="width: 100px;" src=" {{ str_replace('public','/storage',$value->link_signature) }}" ald="Signature"> </td>
                              <td><a href="{{action('SettingController@signature_edit', $value->id)}}" class="btn btn-warning">Edit</a>
                          	</td>
       						  <td><a href="{{action('SettingController@signature_destroy', $value->id)}}" class="btn btn-danger">Delete</a></td>
                          </tr>
                         @endforeach
                         
                    </tbody>
                    </table>
                    <div style="text-align: center;">{{  $listsign->links() }}</div>
                </div>

              
            </div>
          </div>

          @else
            <p>Data tidak ditemukan.</p>
          @endif
        </div> 
    </div>
</div>      
@endsection

<script type="text/javascript">  
$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

  $(".numberOnly").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
  });


  $('.alphaOnly').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str) || e.which == 8 || e.which == 32 || e.which == 46 || e.which == 16 || e.which == 20) {
        return true;
    }

    e.preventDefault();
    return false;
  });

}); 
</script>