@extends('layouts.master')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>
    .sticky {
      position: fixed;
      top: 80;
      left: 0;
      right: 0;
      width: 100%;
      padding-top: 10px;
      z-index: 1000;
      text-align: center;
      -webkit-transition: 0.3s;
      -moz-transition: 0.3s;
      transition: 0.3s;
      background-color: white;
      box-shadow: 0 12px 12px -12px rgba(0, 0, 0, 0.5);
    }
</style>
<script type="text/javascript">
    window.onscroll = function() {myFunction()};

    function myFunction() {
        var navbar = document.getElementById("container-wizard");
        var sticky = navbar.offsetTop;
        if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
      } else {
        navbar.classList.remove("sticky");
      }
    }

</script>

@section('content')
<center>
    <div class="container-cus" id="container-wizard">
        <div class="row">
            <div class="wizard">
                <div class="col-sm-1 col-xs-1">
                    
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/1_sel.png') }}" class="wizard-item-selected-icon">
                    <div class="wizard-item-selected-text"><strong>RESER&shy;VASI</strong></div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/2.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">ISI FORM</div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/3.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">VERIFI&shy;KASI</div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/4.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">WAWAN&shy;CARA</div>
                </div>
                <div class="col-sm-2 col-xs-2">
                    <img src="{{ URL::to('img/5.png') }}" class="wizard-item-icon">
                    <div class="wizard-item-text">PENER&shy;BITAN</div>
                </div>
                <div class="col-sm-1 col-xs-1">
                    
                </div>
            </div>
        </div>
    </div>
    <hr class="line-shadow">
</center>
<div class="container">
    <div class="row justify-content-center">
            <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h6 class="text-uppercase mb-0" style="text-align-last: center;">ALOKASI KUOTA PER TAHUN</h6>
                    <h2 class="text-uppercase mb-0" style="text-align-last: center;">{{ $sumpublished->quota_published + $sumunpublished->quota_unopen }}</h2>
                </div>
                <div class="panel-body">
                    <div class="quota">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-8 col-xs-8 pull-left"><h4><small><i>Kuota Dibuka</i></small></h4></div>
                                    <div class="col-sm-4 col-xs-4 pull-right"><h4 class="font-color-freelancer"><strong>{{ $sumpublished->quota_published }}</strong></h4></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-8 col-xs-8 pull-left"><h4><small><i>Kuota Terambil</i></small></h4></div>
                                    <div class="col-sm-4 col-xs-4 pull-right"><h4 class="font-color-freelancer"><strong>{{ $sumpublished->quota_taken }}</strong></h4></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-8 col-xs-8 pull-left"><h4><small><i>Kuota Tersedia</i></small></h4></div>
                                    <div class="col-sm-4 col-xs-4 pull-right"><h4 class="font-color-freelancer"><strong>{{ $sumpublished->quota_available }}</strong></h4></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-8 col-xs-8 pull-left"><h4><small><i>Kuota Belum Dibuka</i></small></h4></div>
                                    <div class="col-sm-4 col-xs-4 pull-right"><h4 class="font-color-freelancer"><strong>{{ $sumunpublished->quota_unopen }}</strong></h4></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 quota-list">
            <div class="row">
                    @foreach ($kuota as $key => $value)
                    <div class="col-sm-3 col-xs-6" style="text-align-last: center;">
                        <div class="card quota-card">
                            <div class="col-sm-12 col-xs-12"><strong>{{ \Carbon\Carbon::parse($value->tgl_wawancara)->format('d F Y') }}</strong></div>
                            <br style="line-height:10px;">
                            <hr style="border-top: 3px double #8c8b8b;">
                            <div class="col-sm-12 col-xs-12"><small><u>Kuota Tersedia</u></small></div>
                            <div class="col-sm-12 col-xs-12"><strong>{{ $value->kuota_dibuka }}</strong></div>
                            <br>
                            <div class="row">
                                <div class="col-sm-8 col-xs-7" style="text-align-last: left;"><i>Terambil</i>:</div>
                                <div class="col-sm-4 col-xs-5" style="text-align-last: right;"><strong>{{ $value->kuota_diambil }}</strong></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8 col-xs-7" style="text-align-last: left;"><i>Sisa</i>:</div>
                                <div class="col-sm-4 col-xs-5" style="text-align-last: right;"><strong>{{ $value->kuota_tersedia }}</strong></div>
                            </div>
                            <div class="col-sm-12"></div>
                            <hr sty>
                            <?php if ($value->kuota_diambil == $value->kuota_dibuka ) { ?>
                                <input class="btn btn-danger quota-card-button" type="submit" value="HABIS" disabled="disabled" readonly="readonly">
                            <?php } else { 
                                $tanggal=date("Y-m-d",strtotime($value->tgl_wawancara));
                                $id_kuota_wawancara = $value->id_kuota_wawancara;
                                ?>      
                                <?php if($booking_count > 9) { ?>
                                    <form action="{{ route('logout') }}" method="POST" style="margin-bottom: 0px;">           
                                        <input type="submit" class="btn btn-success quota-card-button" name="submit" value="APPLY">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="user_message" value="{{ $user_message }}">
                                        <input type="hidden" name="_method" value="POST">
                                    </form> 
                                <?php } else { ?>
                                    <?php if($id_workflow != 999) { ?>
                                        <input class="btn quota-card-button" type="submit" value="APPLY" disabled="disabled" readonly="readonly" data-toggle="tooltip" title="Permohonan sebelumnya masih aktif dan dalam proses sampai saat ini. Anda bisa melakukan permohonan ulang apabila permohonan Anda sebelumnya tersebut ditolak oleh petugas verifikator."> 
                                    <?php } else { ?>
                                        <button type="button" class="btn btn-success quota-card-button" data-toggle="modal" data-target="#applyPopUp{{ $value->id_kuota_wawancara }}">APPLY</button>
                                    <?php } ?>                          
                                <?php } ?>
                            <?php } ?>

                        </div>
                    </div>        
                    @endforeach
                </div>
        </div>
    </div>
</div>

@foreach($kuota as $key => $value)
  <div id="applyPopUp{{ $value->id_kuota_wawancara }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">RESERVASI TANGGAL {{ \Carbon\Carbon::parse($value->tgl_wawancara)->format('d F Y') }}</h4>
        </div>
        <div class="modal-body">
            <table class="table">
                <tr>
                    <td class="col-md-1 col-sm-1">1.</td>
                    <td class="col-md-11 col-sm-11">Waktu pengisian formulir dibatasi hanya <strong>15 menit</strong>.</td>
                </tr>
                <tr>
                    <td class="col-md-1 col-sm-1">2.</td>
                    <td class="col-md-11 col-sm-11">Pastikan Anda telah mempersiapkan seluruh scan dokumen yang diperlukan seperti KTP, Pas Foto 4x6 berwarna, Akte Kelahiran, Paspor, Sertifikat Pendidikan, Sertifikat IELTS/TOEFL iBT, Surat Keterangan Bank.<br><i>(format dokumen: <strong>.JPEG</strong> / <strong>.JPG</strong> / <strong>.PNG</strong> / <strong>.PDF</strong> , ukuran maksimum per dokumen  <strong>1 MB</strong>)</i></td>
                </tr>
            </table>          
        </div>
        <div class="modal-footer">
            <form action="/addPermohonan" method="post" style="margin-bottom: 0px;">           
                <input type="submit" class="btn btn-success quota-card-button" name="submit" value="APPLY">
                {{ csrf_field() }}
                <input type="hidden" name="tanggal" value="{{$tanggal}}">
                <input type="hidden" name="id_kuota_wawancara" value="{{$id_kuota_wawancara}}">
                <input type="hidden" name="_method" value="POST">
            </form>
        </div>
      </div>
    </div>
  </div>
@endforeach

@endsection


<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   

       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        
    });
  
</script>