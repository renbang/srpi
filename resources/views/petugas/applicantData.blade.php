<script src="vendor/jquery/jquery.min.js"></script>
<link href="/vendor/bootstrap/css/datepicker.css" rel="stylesheet">

<meta name="csrf-token" content="<?php echo csrf_token() ?>">

<style>
  .borderless td, .borderless th {
    border: none;
  }
</style>

@extends('layouts.master')

@section('content')
<center><h2>REVIEW DATA PEMOHON</h2></center>
<hr class="line-shadow">

<div class="container">
  <form action="/verifikasiData" method="post" enctype="multipart/form-data"> 
  <div class="col-md-12">
    <div class="row">
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>A.&emsp;TANGGAL WAWANCARA</strong></div>
          <div class="panel-body">
            <h5>{{ \Carbon\Carbon::parse($data->tgl_wawancara)->format('d F Y') }}</h5>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>B.&emsp;BIODATA</strong></div>
          <div class="panel-body">
            <div class="form-group row">
              <div class="col-sm-5"><h5><small>Nama</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="nama" class="form-control input-sm alphaOnly" id="nama" value="{{ ($data->nama)?$data->nama:'' }}" style="text-transform:uppercase">
                  @if($errors->has('nama'))
                    <p>{{ $errors->first('nama') }}</p>
                  @endif
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-5">
                <h5><small>Jenis Kelamin</small></h5>
              </div>
              <div class="col-sm-7">
                <?php $Lstat = "checked";
                  $Pstat = "";
                  if($data->jns_kel) { 
                    if($data->jns_kel == 'P') {
                      $Lstat = "";
                      $Pstat = "checked";
                    }                         
                  }
                ?>
                <label class="radio-inline">
                  <input type="radio" id="jns_kel" name="jns_kel" value="L" {{ $Lstat }}>Laki-Laki
                </label>
                <label class="radio-inline">
                  <input type="radio" id="jns_kel" name="jns_kel" value="P" {{ $Pstat }}>Perempuan
                </label>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tempat Lahir</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="tmp_lahir" class="form-control input-sm alphaOnly" id="tmp_lahir" value="{{ ($data->tmp_lahir)?$data->tmp_lahir:'' }}" style="text-transform:uppercase">
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tanggal Lahir</small></h5></div>
              <div class="col-sm-7">
                <div class="form-group">
                  <div class="input-group date">
                    <?php 
                      $tgl_lahir_ddmmyyyy = '';
                      if($data->tgl_lahir){
                          $tgl_lahir_yyyyMMdd=$data->tgl_lahir;
                          $tgl_lahir_ddmmyyyy= date('d-m-Y', strtotime($tgl_lahir_yyyyMMdd));
                      } 
                    ?>
                    <input type="text" class="form-control input-sm datepicker" name="tgl_lahir" id="tgl_lahir" value="{{ $tgl_lahir_ddmmyyyy }}" placeholder="dd-mm-yyyy" style="padding-left: 10px" >
                    <div class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                    </div>
                   </div>
                </div>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>NIK</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="nik" class="form-control input-sm numberOnly" maxlength="16" id="nik" value="{{ ($data->nik)?$data->nik:'' }}" style="text-transform:uppercase">
              </div>
            </div>
            <hr>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Nomor Paspor</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="no_paspor" class="form-control input-sm" maxlength="8" id="no_paspor" value="{{ ($data->no_paspor)?$data->no_paspor:'' }}" style="text-transform:uppercase">
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tanggal Diberikan Paspor</small></h5></div>
              <div class="col-sm-7">
                <div class="form-group">
                  <div class="input-group date">
                    <?php 
                      $tgl_diterbitkan_ddmmyyyy = '';
                      if($data->tgl_diterbitkan){
                          $tgl_diterbitkan_yyyyMMdd=$data->tgl_diterbitkan;
                          $tgl_diterbitkan_ddmmyyyy= date('d-m-Y', strtotime($tgl_diterbitkan_yyyyMMdd));
                      } 
                    ?>
                    <input type="text" class="form-control input-sm datepicker" name="tgl_diterbitkan" id="tgl_diterbitkan" value="{{ $tgl_diterbitkan_ddmmyyyy }}" style="padding-left: 10px" >
                    <div class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tanggal Habis Berlaku Paspor</small></h5></div>
              <div class="col-sm-7">
                <div class="form-group">
                  <div class="input-group date">
                    <?php 
                      $tgl_hbs_berlaku_ddmmyyyy = '';
                      if($data->tgl_hbs_berlaku){
                          $tgl_hbs_berlaku_yyyyMMdd=$data->tgl_hbs_berlaku;
                          $tgl_hbs_berlaku_ddmmyyyy= date('d-m-Y', strtotime($tgl_hbs_berlaku_yyyyMMdd));
                      } 
                    ?>
                    <input type="text" class="form-control input-sm datepicker" name="tgl_hbs_berlaku" id="tgl_hbs_berlaku" value="{{ $tgl_hbs_berlaku_ddmmyyyy }}" style="padding-left: 10px" >
                    <div class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                    </div>
                   </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Alamat Lengkap</small></h5></div>
              <div class="col-sm-7">
                <textarea name="alamat" class="form-control input-sm" rows="5" id="alamat" style="text-transform:uppercase">{{ ($data->alamat)?$data->alamat:'' }}</textarea>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Provinsi</small></h5></div>
              <div class="col-sm-7">
                <select  class="form-control input-sm" name="provinsi" id="provinsi">  
                  <option value="0"  selected="true">Pilih Provinsi</option>  
                  @foreach ($listpropinsi as $propinsi)
                    <option value="{{ $propinsi->provinsi }}">{{ $propinsi->provinsi }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Kota/Kabupaten</small></h5></div>
              <div class="col-sm-7">
                <select class="form-control input-sm" name="kabupaten" id="kabupaten">                    
                  <option value="0" disable="true" selected="true">Pilih Kota/Kabupaten</option>
                  @if($lokasi)
                    @foreach ($listkabupaten as $kabupaten)
                      <option value="{{ $kabupaten->kabupaten }}">{{ $kabupaten->kabupaten }}</option>
                    @endforeach
                  @endif
                </select>      
             </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Kecamatan</small></h5></div>
              <div class="col-sm-7">
                <select class="form-control input-sm" name="kecamatan" id="kecamatan">
                  <option value="0" disable="true" selected="true">Pilih Kecamatan</option>
                  @if($lokasi)
                    @foreach ($listkecamatan as $kecamatan)
                      <option value="{{ $kecamatan->kecamatan }}">{{ $kecamatan->kecamatan }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Kelurahan/Desa</small></h5></div>
              <div class="col-sm-7">
                <select class="form-control input-sm" name="kelurahan" id="kelurahan">
                  <option value="0" disable="true" selected="true">Pilih Kelurahan</option>
                  @if($lokasi)
                    @foreach ($listkelurahan as $kelurahan)
                      <option value="{{ $kelurahan->kd_lokasi }}">{{ $kelurahan->kelurahan }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
            <hr>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Email</small></h5></div>
              <div class="col-sm-7">
                <input type="email" name="email" class="form-control input-sm" id="email" value="{{ ($data->no_tlp)?$data->email:'' }}" >
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Nomor Handphone / Telepon</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="no_tlp" class="form-control input-sm numberOnly" id="no_tlp" value="{{ ($data->no_tlp)?$data->no_tlp:'' }}" style="text-transform:uppercase" >
              </div>
            </div>
          </div>
        </div>
      </div> <!--end of left side-->
      
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>C.&emsp;PENDIDIKAN</strong></div>
          <div class="panel-body">
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tempat Kuliah</small></h5></div>
              <div class="col-sm-7">
                <?php $DNstat = "checked";
                  $LNstat = "";
                  if($data->tmp_kuliah) { 
                    if($data->tmp_kuliah == 'Dalam Negeri') {
                      $DNstat = "checked";
                      $LNstat = "";
                    }                         
                  }
                ?>
                <label class="radio-inline">
                  <input type="radio" id="tmp_kuliah" name="tmp_kuliah" value="Dalam Negeri" {{ $DNstat }}>Dalam Negeri
                </label>
                <label class="radio-inline">
                  <input type="radio" id="tmp_kuliah" name="tmp_kuliah" value="Luar Negeri" {{ $LNstat }}>Luar Negeri
                </label>
             </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Status Mahasiswa</small></h5></div>
              <div class="col-sm-7">
                <select  class="form-control input-sm" name="kd_status_mahasiswa" id="kd_status_mahasiswa">  
                  <option value="0"  selected="true">Pilih Status</option>   
                  @foreach ($liststatusmhs as $statusmhs)
                    <option value="{{ $statusmhs->kd_status_mahasiswa }}">{{ $statusmhs->desc_status_mahasiswa }}</option>
                  @endforeach
                </select>
             </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Jenjang</small></h5></div>
              <div class="col-sm-7">
                <select  class="form-control input-sm" name="kd_jenjang" id="kd_jenjang">  
                  <option value="0"  selected="true">Pilih Jenjang</option>   
                  @foreach ($listjenjang as $jenjang)
                    <option value="{{ $jenjang->kd_jenjang }}">{{ $jenjang->desc_jenjang }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Perguruan Tinggi</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="nm_universitas" class="form-control input-sm" maxlength="100" id="nm_universitas" value="{{ ($data->nm_universitas)?$data->nm_universitas:'' }}" style="text-transform:uppercase"  >
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Fakultas</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="fakultas" class="form-control input-sm" maxlength="150" id="fakultas" value="{{ ($data->fakultas)?$data->fakultas:'' }}" style="text-transform:uppercase">
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Jurusan</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="jurusan" class="form-control input-sm" maxlength="150" id="jurusan" value="{{ ($data->jurusan)?$data->jurusan:'' }}" style="text-transform:uppercase"  >
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Lulus Tahun</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="thn_lulus" class="form-control input-sm numberOnly" maxLength="4" id="thn_lulus" value="{{ ($data->thn_lulus)?$data->thn_lulus:'' }}" style="text-transform:uppercase" >
              </div>
            </div>
            
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Alamat Perguruan Tinggi</small></h5></div>
              <div class="col-sm-7">
                <textarea name="alamat_universitas" class="form-control input-sm" rows="5" id="alamat_universitas" style="text-transform:uppercase" >{{ ($data->alamat_universitas)?$data->alamat_universitas:'' }}</textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>D.&emsp;IELTS / TOEFL iBT</strong></div>
          <div class="panel-body">
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Nilai/Skor</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="skor" class="form-control input-sm numberOnly" id="skor" value="{{ ($data->skor)?$data->skor:'' }}" style="text-transform:uppercase" >
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tanggal Test</small></h5></div>
              <div class="col-sm-7">
                <div class="form-group">
                  <div class="input-group date">
                    <?php 
                      $tgl_test_ddmmyyyy = '';
                      if($data->tgl_test){
                          $tgl_test_yyyyMMdd=$data->tgl_test;
                          $tgl_test_ddmmyyyy= date('d-m-Y', strtotime($tgl_test_yyyyMMdd));
                      } 
                    ?>
                    <input type="text" class="form-control input-sm datepicker" name="tgl_test" id="tgl_test" value="{{ $tgl_test_ddmmyyyy }}" style="padding-left: 10px" >
                    <div class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>No. Identitas Kandidat</small></h5></div>
              <div class="col-sm-7">
                <input type="text" name="no_identitas_kandidat" class="form-control input-sm" id="no_identitas_kandidat" value="{{ ($data->no_identitas_kandidat)?$data->no_identitas_kandidat:'' }}" style="text-transform:uppercase" >
              </div>
            </div>
          </div>            
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>E.&emsp;TUJUAN KEDATANGAN SELAMA DI AUSTRALIA</strong></div>
          <div class="panel-body">
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Deskripsi</small></h5></div>
              <div class="col-sm-7">
                <textarea name="alasan_kedatangan" class="form-control input-sm" rows="10" id="alasan_kedatangan" >{{ ($data->alasan_kedatangan)?$data->alasan_kedatangan:'' }}</textarea>
              </div>
            </div>
          </div>
        </div>                     
      </div><!--end of right side-->
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>F.&emsp;LAMPIRAN DOKUMEN</strong></div>
          <div class="panel-body">
            <div class="col-sm-6">
              <?php $no = 0;
                for ($i=0 ; $i<count($ms_lampiran)/2 ; $i++) {
                  $flag = 0;
                  for ($j=0 ; $j<count($list_lampiran) ; $j++) {
                    if ($ms_lampiran[$i]->kd_lampiran == $list_lampiran[$j]->kd_lampiran) {
                      $flag = 0;
                 ?>
                  <div class="row form-input" style="padding-bottom: 10px">
                    <div class="col-sm-8"><h5>{{ $i+1 }}. &emsp; <i>{{ $ms_lampiran[$i]->desc_lampiran }}</i></h5></div>
                    <div class="col-sm-3"> 
                      <form action="/downloadDokumenPemohon" method="POST" style="margin-bottom: 0px;">           
                        <input type="submit" class="btn btn-sm btn-success quota-card-button" name="submit" value="LIHAT">
                        {{ csrf_field() }}
                        <input type="hidden" name="link" value="{{ $list_lampiran[$j]->link }}">
                        <input type="hidden" name="_method" value="POST">
                      </form>
                    </div>
                  </div>
              <?php break; }
                    else {
                      $flag++;
                    }
                    if($j+1 == count($list_lampiran) && $flag > 0) { ?>
                      <div class="row form-input" style="padding-bottom: 10px">
                        <div class="col-sm-8"><h5>{{ $i+1 }}. &emsp; <i>{{ $ms_lampiran[$i]->desc_lampiran }}</i></h5></div>
                        <div class="col-sm-3"> 
                          <input type="submit" class="btn btn-sm btn-default quota-card-button" name="submit" value="TIDAK ADA" readonly="read" disabled="disabled">
                        </div>
                      </div>
              <?php $no = $i + 1;
                    break;
                    }
                  }
                }
              ?>  
            </div>
            <div class="col-sm-6">
              <?php
                for ($i=$no ; $i<count($ms_lampiran) ; $i++) {
                  $flag = 0;
                  for ($j=0 ; $j<count($list_lampiran) ; $j++) {
                    if ($ms_lampiran[$i]->kd_lampiran == $list_lampiran[$j]->kd_lampiran) {
                      $flag = 0;
                 ?>
                  <div class="row form-input" style="padding-bottom: 10px">
                    <div class="col-sm-8"><h5>{{ $i+1 }}. &emsp; <i>{{ $ms_lampiran[$i]->desc_lampiran }}</i></h5></div>
                    <div class="col-sm-3"> 
                      <form action="/downloadDokumenPemohon" method="POST" style="margin-bottom: 0px;">           
                        <input type="submit" class="btn btn-sm btn-success quota-card-button" name="submit" value="LIHAT">
                        {{ csrf_field() }}
                        <input type="hidden" name="link" value="{{ $list_lampiran[$j]->link }}">
                        <input type="hidden" name="_method" value="POST">
                      </form>
                    </div>
                  </div>
              <?php break; }
                    else {
                      $flag++;
                    }
                    if($j+1 == count($list_lampiran) && $flag > 0) { ?>
                      <div class="row form-input" style="padding-bottom: 10px">
                        <div class="col-sm-8"><h5>{{ $i+1 }}. &emsp; <i>{{ $ms_lampiran[$i]->desc_lampiran }}</i></h5></div>
                        <div class="col-sm-3"> 
                          <input type="submit" class="btn btn-sm btn-default quota-card-button" name="submit" value="TIDAK ADA" readonly="read" disabled="disabled">
                        </div>
                      </div>
              <?php $no = $i;
                    break;
                    }
                  }
                }
              ?>  
            </div> 
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><strong>STATUS</strong></div>
          <div class="panel-body">
            <center>
              <div class="col-md-12 col-sm-12">
                <?php if ($activity) { ?>
                  <?php if ($activity->status == 'Y') { ?>
                    <button type="button" class="btn btn-lg btn-success" disabled="disabled" readonly="readonly">DATA DITERIMA &emsp; <i class="fa fa-check-circle"></i></button>
                    <hr>
                    <table class="table borderless">
                      <tr>
                        <td><h4><small>Tanggal</small></h4></td>
                        <td><h5><strong>{{ \Carbon\Carbon::parse($activity->activity_at)->format('d F Y') }}</strong></h5></td>
                      </tr>
                      <tr>
                        <td><h4><small>Pukul</small></h4></td>
                        <td><h5><strong>{{  \Carbon\Carbon::parse($activity->activity_at)->format('H:i:s') }}</strong></h5></td>
                      </tr>
                      <tr>
                        <td><h4><small>Verifikator</small></h4></td>
                        <td><h5><strong>{{ $activity->name }}</strong></h5></td>
                      </tr>
                      <tr>
                        <td><h4><small>Alasan</small></h4></td>
                        <td><h5><strong>{{ ($activity->keterangan != null)?$activity->keterangan:'-' }}</strong></h5></td>
                      </tr>
                    </table>
                  <?php } elseif ($activity->status == 'N') { ?>
                    <button type="button" class="btn btn-lg btn-danger" disabled="disabled" readonly="readonly">DATA DITOLAK &emsp; <i class="fa fa-times-circle"></i></button>
                    <hr>
                    <table class="table borderless">
                      <tr>
                        <td><h4><small>Tanggal</small></h4></td>
                        <td><h5><strong>{{ \Carbon\Carbon::parse($activity->activity_at)->format('d F Y') }}</strong></h5></td>
                      </tr>
                      <tr>
                        <td><h4><small>Pukul</small></h4></td>
                        <td><h5><strong>{{  \Carbon\Carbon::parse($activity->activity_at)->format('H:i:s') }}</strong></h5></td>
                      </tr>
                      <tr>
                        <td><h4><small>Verifikator</small></h4></td>
                        <td><h5><strong>{{ $activity->name }}</strong></h5></td>
                      </tr>
                      <tr>
                        <td><h4><small>Alasan</small></h4></td>
                        <td><h5><strong>{{ ($activity->keterangan != null)?$activity->keterangan:'-' }}</strong></h5></td>
                      </tr>
                    </table>
                  <?php } else { ?>                      
                      <div class="form-group row">    
                        <div class="col-sm-3"><h4><small>Review Wawancara</small></h4></div>
                        <div class="col-sm-8">
                          <textarea name="keterangan" class="form-control input-sm" rows="5" id="keterangan"></textarea>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-lg btn-danger" name="status" value="N">TOLAK &emsp; <i class="fa fa-times-circle"></i></button>&emsp;
                      <button type="submit" class="btn btn-lg btn-success" name="status" value="Y">TERIMA & TERBITKAN E-SRPI &emsp; <i class="fa fa-check-circle"></i></button>
                      {{ csrf_field() }}
                      <input type="hidden" name="id_user" value="{{ $data->id_user }}">
                      <input type="hidden" name="id_activity" value="{{ $activity->id_activity }}">
                      <input type="hidden" name="id_kuota_wawancara" value="{{ $data->id_kuota_wawancara }}">
                      <input type="hidden" name="no_tiket" value="{{ $data->no_tiket }}">
                      <input type="hidden" name="_method" value="POST">
                  <?php } ?>
                <?php } ?>
              </div>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>
</div>
<!-- <div id="tolakVerifikasi" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Alasan Penolakan</h4>
        </div>
        <div class="modal-body">
            {{ csrf_field() }}
            <textarea name="keterangan" class="form-control" rows="5" id="keterangan"></textarea>
            
            <input type="hidden" name="_method" value="POST">
        </div>
        <div class="modal-footer">
          <center>
            <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">BATAL</button> &emsp;
            {{ csrf_field() }}
            <input type="hidden" name="id_user" value="{{ $data->id_user }}">
            <input type="hidden" name="id_activity" value="{{ $activity->id_activity }}">
            <input type="hidden" name="id_kuota_wawancara" value="{{ $data->id_kuota_wawancara }}">
            <input type="hidden" name="no_tiket" value="{{ $data->no_tiket }}">
            <input type="hidden" name="status" value="N">
            <input type="hidden" name="_method" value="POST">
            <button type="submit" class="btn btn-danger btn-lg" name="submit">TOLAK &emsp; <i class="fa fa-times-circle"></i></button>
          </center>
        </div>
    </div>
  </div>
</div> -->

@endsection

<script>
  $(document).ready(function(){
    $('#submit').hide();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });  

    $(".numberOnly").keypress(function (e) {
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
      }
    });


    $('.alphaOnly').keypress(function (e) {
      var regex = new RegExp("^[a-zA-Z0-9]+$");
      var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
      if (regex.test(str) || e.which == 8 || e.which == 32 || e.which == 46 || e.which == 16 || e.which == 20) {
          return true;
      }

      e.preventDefault();
      return false;
    });

  }); 

  $(function(){
    
    $('#provinsi').change(function(e){
        var provinsi = e.target.value;
        console.log(provinsi);

        if(provinsi){

          $.ajax({
             type:"GET",
             url:"{{url('api/get-kabupaten-list')}}?provinsi="+provinsi,
             success:function(res){  
              if(res){

                  $("#kabupaten").empty();
                  $("#kabupaten").append('<option value="0" disable="true" selected="true">Pilih Kota/Kabupaten</option>');

                  $('#kecamatan').empty();
                  $('#kecamatan').append('<option value="0" disable="true" selected="true">Pilih Kecamatan</option>');

                  $('#kelurahan').empty();
                  $('#kelurahan').append('<option value="0" disable="true" selected="true">Pilih Kelurahan</option>');


                  $.each(res,function(index,Obj){
                      $("#kabupaten").append('<option value="'+Obj.kabupaten+'">'+Obj.kabupaten+'</option>');
                  });
             
              }else{
                 $("#kabupaten").empty();
                 $("#kecamatan").empty();
                 $("#kelurahan").empty();

              }
             }
          });
      }else{
          $("#kabupaten").empty();
      }      

    });


    $('#kabupaten').change(function(e){
        var kabupaten = e.target.value;
        console.log(kabupaten);

        if(kabupaten){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kecamatan-list')}}?kabupaten="+kabupaten,
             success:function(res){              
              if(res){                  

                  $('#kecamatan').empty();
                  $('#kecamatan').append('<option value="0" disable="true" selected="true">Pilih Kecamatan</option>');

                  $.each(res,function(index,Obj){
                      $("#kecamatan").append('<option value="'+Obj.kecamatan+'">'+Obj.kecamatan+'</option>');
                  });
             
              }else{
                 $("#kecamatan").empty();
              }
             }
          });
      }else{
          $("#kecamatan").empty();
      }      

    });

    $('#kecamatan').change(function(e){
        var kecamatan = e.target.value;
        console.log(kecamatan);

        if(kecamatan){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kelurahan-list')}}?kecamatan="+kecamatan,
             success:function(res){               
              if(res){                  

                  $('#kelurahan').empty();
                  $('#kelurahan').append('<option value="0" disable="true" selected="true">Pilih Kelurahan</option>');

                  $.each(res,function(index,Obj){
                      $("#kelurahan").append('<option value="'+Obj.kd_lokasi+'">'+Obj.kelurahan+'</option>');
                  });
             
              }else{
                 $("#kelurahan").empty();
              }
             }
          });
        }
        else{
          $("#kelurahan").empty();
        }      
    });
  });

  $(function(){
    $.fn.datepicker.defaults.autoclose = true;
    $(".datepicker").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayHighlight: true
    });

  });

  $(function(){  
    <?php if( $lokasi ){ ?>
      $('[name=provinsi]').val('{{ $lokasi->provinsi }}');
      $('[name=kabupaten]').val('{{ $lokasi->kabupaten }}');
      $('[name=kecamatan]').val('{{ $lokasi->kecamatan }}');
      $('[name=kelurahan]').val('{{ $lokasi->kd_lokasi }}');
    <?php } ?>
  });

  $(function(){  
    <?php if( $data->kd_jenjang ){ ?>
      $('[name=kd_jenjang]').val('{{ $data->kd_jenjang }}');
    <?php } ?>
  });

  $(function(){  
    <?php if( $data->kd_status_mahasiswa ){ ?>
      $('[name=kd_status_mahasiswa]').val('{{ $data->kd_status_mahasiswa }}');
    <?php } ?>
  });
</script>