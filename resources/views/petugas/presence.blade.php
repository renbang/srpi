<script src="vendor/jquery/jquery.min.js"></script>
<link href="/vendor/bootstrap/css/datepicker.css" rel="stylesheet">

<script type="text/javascript">
  function filterField() {
    if (document.getElementById('filter-checkbox').checked) {
       document.getElementById('filter-by-select').removeAttribute("disabled");
       document.getElementById('filter-by-input').removeAttribute("disabled");
       document.getElementById('filter-by-button').removeAttribute("disabled");
    } else {
       document.getElementById('filter-by-select').setAttribute("disabled","disabled");
       document.getElementById('filter-by-input').setAttribute("disabled","disabled");
       document.getElementById('filter-by-button').setAttribute("disabled","disabled");
    }
  }

  function getDataByDate(tgl_wawancara) {
    window.location.href = "/kehadiran/" + tgl_wawancara;
  }
</script>

@extends('layouts.master')

@section('content')
<center><h2>KONFIRMASI KEHADIRAN PESERTA</h2></center>
<hr class="line-shadow">
<div class="container">
  <div class="col-md-12">
    <div class="row">
        <div class="col-sm-12 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-body" style="background-color: #c1fff3;">
              <div class="col-sm-6 col-xs-12">
                <div class="col-sm-12 col-xs-12">
                  <form method="GET" action="/kehadiran">
                    <select class="form-control input-sm btn btn-default" name="tgl_wawancara" id="tgl_wawancara" onchange="getDataByDate(this.value)">
                      <?php
                        $system_time = Carbon\Carbon::now();
                        $current_date = $system_time->toDateString();
                        $selected_option = 0;
                        $counter = 0;
                      ?>
                      @foreach ($list_tanggal as $tanggal)
                        <?php if($tgl_wawancara == $tanggal->tgl_wawancara) { 
                          $selected_option = 1;
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}" selected="true">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } elseif($tanggal->tgl_wawancara >= $current_date && $selected_option == 0) { 
                          $selected_option = 1;
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}" selected="true">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } elseif ($tanggal->tgl_wawancara < $current_date && $selected_option == 0) {
                          if ($counter+1 == count($list_tanggal) && $selected_option == 0) {
                          $selected_option = 1;
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}" selected="true">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } else {
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } } else {
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } $counter++; ?>
                      @endforeach
                    </select>
                  </form>
                </div>
              </div>
              <div class="col-sm-6 col-xs-12">
                <div class="col-sm-12 col-xs-12">
                  <div class="input-group">
                    <div class="input-group-btn checkbox">
                      <label>
                        <input type="checkbox" id="filter-checkbox" onclick="filterField()">
                      </label>
                    </div>
                    <div class="input-group-btn">
                      <select class="btn btn-default" name="filter_by" id="filter-by-select" disabled="disabled">
                        <option selected="true">- Filter by -</option>  
                        <option value="no_tiket">No. Tiket</option>
                        <option value="nama">Nama</option>
                        <option value="nik">NIK</option>
                        <option value="no_paspor">No. Paspor</option>
                      </select>
                    </div>
                    <input class="form-control" id="filter-by-input" type="text" disabled="disabled" placeholder="Filter data">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-default" id="filter-by-button" disabled="disabled">Apply</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel-body" style="background-color: #f9f9f9;">
              <div class="col-sm-8 col-xs-12">
                <table>
                  <tr>
                    <td class="col-md-6"><h4><small>TANGGAL WAWANCARA</small></h4></td>
                    <td class="col-md-6"><h5>{{ \Carbon\Carbon::parse($tgl_wawancara)->format('d F Y') }}</h5></td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><h4><small>PETUGAS</small></h4></td>
                    <td class="col-md-6"><h5>{{ $petugas }} &emsp; <i class="fa fa-check-circle"></i></h5><td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><h4><small>Filter by</small></h4></td>
                    <td class="col-md-6"><h5>-</h5><td>
                  </tr>
                </table>
              </div>
              <div class="col-sm-4 col-xs-12">
                <table>
                  <tr>
                    <td class="col-md-6"><i>Belum Hadir</i></td>
                    <td class="col-md-6" style="text-align: right;"><strong>{{ $count_belum_hadir }}</strong></td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><i>Sudah Hadir</i></td>
                    <td class="col-md-6" style="text-align: right;"><strong>{{ $count_sudah_hadir }}</strong></td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <hr style="border-top: 1px double #18BC9C; margin: 5px;">
                    </td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><i>Total</i></td>
                    <td class="col-md-6" style="text-align: right;"><strong>{{ $count_total }}</strong></td>
                  </tr>
                </table>
              </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Nomor Tiket</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>NIK</th>
                        <th>No. Paspor</th>
                        <th>Konfirmasi Kehadiran</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; ?>
                        @foreach($list_kehadiran as $key => $kehadiran)
                          <tr>
                            <td> {{ $no++ . '.' }} </td>
                            <td> {{ str_pad($kehadiran->no_tiket, 13, '0', STR_PAD_LEFT) }} </td>
                            <td> {{ $kehadiran->nama }} </td>
                            <td> {{ ($kehadiran->jns_kel == 'P')?'PEREMPUAN':'LAKI-LAKI' }} </td>
                            <td> {{ $kehadiran->nik }} </td>
                            <td> {{ $kehadiran->no_paspor }} </td>
                            <td>
                              <?php if($kehadiran->id_workflow <= 4) { ?>
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#konfirmasiHadir{{ $kehadiran->no_tiket }}">KONFIRMASI HADIR</button>
                              <?php } else { ?>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#konfirmasiHadir{{ $kehadiran->no_tiket }}">{{ \Carbon\Carbon::parse($kehadiran->activity_at_interview)->addHours(7)->format('H:i:s') }} WIB &emsp; <i class="fa fa-check-circle"></i></button>
                              <?php } ?>
                            </td>
                            <td>
                              <?php if($kehadiran->id_workflow <= 4) { ?>
                                <i>BELUM HADIR</i>
                              <?php } elseif($kehadiran->id_workflow == 5) { ?>
                                <i>PROSES</i>
                              <?php } elseif($kehadiran->id_workflow == 6) { ?>
                                <h5><strong style="color: #34aa68;">SELESAI</strong></h5>
                              <?php } ?>
                            </td>
                          </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{ csrf_field() }} 
    </div>
  </div>  
</div>

@foreach($list_kehadiran as $key => $kehadiran)
  <div id="konfirmasiHadir{{ $kehadiran->no_tiket }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">KONFIRMASI KEHADIRAN</h4>
        </div>
        <div class="modal-body">
          <center>
            <div class="check-in-photo-div">
              <img class="check-in-photo" src="{{ str_replace('public','/storage', $kehadiran->link) }}" alt="pas foto peserta">
            </div>
          </center>
          <br>
          <table class="table table-hover">
            <tbody>
              <tr>
                <td class="col-sm-3"><h5><small>NOMOR TIKET</small></h5></td>
                <td class="col-sm-9"><h5>{{ $kehadiran->no_tiket }}</h5></td>
              </tr>
              <tr>
                <td class="col-sm-3"><h5><small>TANGGAL WAWANCARA</small></h5></td>
                <td class="col-sm-9"><h5>{{ $kehadiran->tgl_wawancara }}</h5></td>
              </tr>
              <tr>
                <td class="col-sm-3"><h5><small>NAMA</small></h5></td>
                <td class="col-sm-9"><h5>{{ $kehadiran->nama }}</h5></td>
              </tr>
              <tr>
                <td class="col-sm-3"><h5><small>JENIS KELAMIN</small></h5></td>
                <td class="col-sm-9"><h5>{{ ($kehadiran->jns_kel == 'P')?'PEREMPUAN':'LAKI-LAKI' }}</h5></td>
              </tr>
              <tr>
                <td class="col-sm-3"><h5><small>NIK</small></h5></td>
                <td class="col-sm-9"><h5>{{ $kehadiran->nik }}</h5></td>
              </tr>
              <tr>
                <td class="col-sm-3"><h5><small>NOMOR PASPOR</small></h5></td>
                <td class="col-sm-9"><h5>{{ $kehadiran->no_paspor }}</h5></td>
              </tr>
              <tr>
                <td class="col-sm-3"><h5><small>TEMPAT, TANGGAL LAHIR</small></h5></td>
                <td class="col-sm-9"><h5>{{ $kehadiran->tmp_lahir . ', ' . \Carbon\Carbon::parse($kehadiran->tgl_lahir)->format('d F Y') }}</h5></td>
              </tr>
              <tr>
                <td class="col-sm-3"><h5><small>PENDIDIKAN</small></h5></td>
                <td class="col-sm-9"><h5>{{ $kehadiran->desc_jenjang . ' - ' . $kehadiran->desc_status_mahasiswa }}
                  <br>
                  {{ $kehadiran->fakultas . ', ' . $kehadiran->jurusan }}
                  <br>
                  {{ $kehadiran->nm_universitas }}
                </h5></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer" style="background-color: #dff0d8">
          <center>
            <?php if($kehadiran->id_workflow <= 4) { ?>
              <form action="/konfirmasiKehadiran" method="post" style="margin-bottom: 0px;">       
                <button type="button" class="btn btn-lg  btn-default" data-dismiss="modal">TUTUP</button>
                <input type="hidden" name="id_user" value="{{ $kehadiran->id_user }}">
                <input type="hidden" name="id_kuota_wawancara" value="{{ $kehadiran->id_kuota_wawancara }}">
                <input type="hidden" name="tgl_wawancara" value="{{ $tgl_wawancara }}">
                <input type="hidden" name="no_tiket" value="{{ $kehadiran->no_tiket }}">
                {{ csrf_field() }}
                <input type="submit" class="btn btn-lg btn-success" name="submit" value="KONFIRMASI PENDAFTAR HADIR">
                <input type="hidden" name="_method" value="POST">
              </form>
            <?php } else  { ?>
              <h5>Pendaftar sudah hadir untuk wawancara <i class="fa fa-check-circle"></i><br> pada <u style="color: #34aa68;"><i>{{ \Carbon\Carbon::parse($kehadiran->activity_at_interview)->addHours(7)->format('d F Y') }}</i></u> pukul <u style="color: #34aa68;"><i>{{ \Carbon\Carbon::parse($kehadiran->activity_at_interview)->addHours(7)->format('H:i:s') }} WIB</i></u></h5>
            <?php } ?>
          </center>
        </div>
      </div>
    </div>
  </div>
@endforeach

@endsection