<script src="vendor/jquery/jquery.min.js"></script>
<link href="/vendor/bootstrap/css/datepicker.css" rel="stylesheet">

<script type="text/javascript">
  function filterField() {
    if (document.getElementById('filter-checkbox').checked) {
       document.getElementById('filter-by-select').removeAttribute("disabled");
       document.getElementById('filter-by-input').removeAttribute("disabled");
       document.getElementById('filter-by-button').removeAttribute("disabled");
    } else {
       document.getElementById('filter-by-select').setAttribute("disabled","disabled");
       document.getElementById('filter-by-input').setAttribute("disabled","disabled");
       document.getElementById('filter-by-button').setAttribute("disabled","disabled");
    }
  }

  function getDataByDate(tgl_wawancara) {
    window.location.href = "/verdata/" + tgl_wawancara;
  }
</script>

@extends('layouts.master')

@section('content')
<center><h2>REVIEW DATA PEMOHON</h2></center>
<hr class="line-shadow">
<div class="container">
  <div class="col-md-12">
    <div class="row">
        <div class="col-sm-12 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-body" style="background-color: #c1fff3;">
              <div class="col-sm-6 col-xs-12">
                <div class="col-sm-12 col-xs-12">
                  <form method="GET" action="/verdata">
                    <select class="form-control input-sm btn btn-default" name="tgl_wawancara" id="tgl_wawancara" onchange="getDataByDate(this.value)">
                      <?php
                        $system_time = Carbon\Carbon::now();
                        $current_date = $system_time->toDateString();
                        $selected_option = 0;
                        $counter = 0;
                      ?>
                      @foreach ($list_tanggal as $tanggal)
                        <?php if($tgl_wawancara == $tanggal->tgl_wawancara) { 
                          $selected_option = 1;
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}" selected="true">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } elseif($tanggal->tgl_wawancara >= $current_date && $selected_option == 0) { 
                          $selected_option = 1;
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}" selected="true">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } elseif ($tanggal->tgl_wawancara < $current_date && $selected_option == 0) {
                          if ($counter+1 == count($list_tanggal) && $selected_option == 0) {
                          $selected_option = 1;
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}" selected="true">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } else {
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } } else {
                        ?>
                          <option value="{{ $tanggal->tgl_wawancara }}" id="{{ $tanggal->tgl_wawancara }}">{{ \Carbon\Carbon::parse($tanggal->tgl_wawancara)->format('d F Y') }}</option>
                        <?php } $counter++; ?>
                      @endforeach
                    </select>
                  </form>
                </div>
              </div>
              <div class="col-sm-6 col-xs-12">
                <div class="col-sm-12 col-xs-12">
                  <div class="input-group">
                    <div class="input-group-btn checkbox">
                      <label>
                        <input type="checkbox" id="filter-checkbox" onclick="filterField()">
                      </label>
                    </div>
                    <div class="input-group-btn">
                      <select class="btn btn-default" name="filter_by" id="filter-by-select" disabled="disabled">
                        <option selected="true">- Filter by -</option>  
                        <option value="no_tiket">No. Tiket</option>
                        <option value="nama">Nama</option>
                        <option value="nik">NIK</option>
                        <option value="no_paspor">No. Paspor</option>
                      </select>
                    </div>
                    <input class="form-control" id="filter-by-input" type="text" disabled="disabled" placeholder="Filter data">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-default" id="filter-by-button" disabled="disabled">Apply</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel-body" style="background-color: #f9f9f9;">
              <div class="col-sm-8 col-xs-12">
                <table>
                  <tr>
                    <td class="col-md-6"><h4><small>TANGGAL WAWANCARA</small></h4></td>
                    <td class="col-md-6"><h5>{{ \Carbon\Carbon::parse($tgl_wawancara)->format('d F Y') }}</h5></td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><h4><small>VERIFIKATOR</small></h4></td>
                    <td class="col-md-6"><h5>{{ $verifikator }} &emsp; <i class="fa fa-check-circle"></i></h5><td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><h4><small>Filter by</small></h4></td>
                    <td class="col-md-6"><h5>-</h5><td>
                  </tr>
                </table>
              </div>
              <div class="col-sm-4 col-xs-12">
                <table>
                  <tr>
                    <td class="col-md-6"><i>Diterima</i></td>
                    <td class="col-md-6" style="text-align: right;"><strong>{{ $count_diterima }}</strong></td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><i>Ditolak</i></td>
                    <td class="col-md-6" style="text-align: right;"><strong>{{ $count_ditolak }}</strong></td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><i>Belum Diverifikasi</i></td>
                    <td class="col-md-6" style="text-align: right;"><strong>{{ $count_belum_diverifikasi }}</strong></td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <hr style="border-top: 1px double #18BC9C; margin: 5px;">
                    </td>
                  </tr>
                  <tr>
                    <td class="col-md-6"><i>Total</i></td>
                    <td class="col-md-6" style="text-align: right;"><strong>{{ $count_total }}</strong></td>
                  </tr>
                </table>
              </div>
              </div>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Nomor Tiket</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>NIK</th>
                        <th>No. Paspor</th>
                        <th>Status Verifikasi</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; ?>
                        @foreach($list_data as $key => $data)
                          <tr>
                            <td> {{ $no++ . '.' }} </td>
                            <td> {{ str_pad($data->no_tiket, 13, '0', STR_PAD_LEFT) }} </td>
                            <td> {{ $data->nama }} </td>
                            <td> {{ ($data->jns_kel == 'P')?'PEREMPUAN':'LAKI-LAKI' }} </td>
                            <td> {{ $data->nik }} </td>
                            <td> {{ $data->no_paspor }} </td>
                            <td>
                              <?php if($data->id_workflow == 6) { ?>
                                <strong style="color: #34aa68;"><i class="fa fa-check-circle"></i> &emsp; E-SRPI TERBIT</strong>
                              <?php } elseif ($data->id_workflow == 5) { ?>
                                <?php if ($data->status == "N") { ?>
                                  <strong style="color: #a93933;"><i class="fa fa-times-circle"></i> &emsp; DITOLAK</strong>
                                <?php } elseif($data->status == null) { ?>
                                  <i>BELUM DIVERIFIKASI</i>
                                <?php } else { ?>
                                  <strong style="color: #34aa68;"><i class="fa fa-check-circle"></i> &emsp; DITERIMA</strong>
                                <?php } ?>
                              <?php } ?>
                            </td>
                            <td>
                              <form action="/dataPemohon" method="post" style="margin-bottom: 0px;">           
                                <button type="submit" class="btn btn-default" name="submit">LIHAT &emsp; <i class="fa fa-file-o"></i></button>
                                {{ csrf_field() }}
                                <input type="hidden" name="id_user" value="{{ $data->id_user }}">
                                <input type="hidden" name="id_kuota_wawancara" value="{{ $data->id_kuota_wawancara }}">
                                <input type="hidden" name="no_tiket" value="{{ $data->no_tiket }}">
                                <input type="hidden" name="_method" value="POST">
                              </form>
                            </td>
                          </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{ csrf_field() }} 
    </div>
  </div>  
</div>

@endsection