<script src="vendor/jquery/jquery.min.js"></script>
<style>
  .borderless td, .borderless th {
    border: none;
  }
</style>

@extends('layouts.master')

@section('content')
<center><h2>VERIFIKASI DOKUMEN PEMOHON</h2></center>
<hr class="line-shadow">
<div class="container">
  <div class="col-md-12">
    <div class="row">
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>A.&emsp;TANGGAL WAWANCARA</strong></div>
          <div class="panel-body">
            <h5>{{ \Carbon\Carbon::parse($dokumen->tgl_wawancara)->format('d F Y') }}</h5>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>B.&emsp;BIODATA</strong></div>
          <div class="panel-body">
            <div class="form-group row">
              <div class="col-sm-5"><h5><small>Nama</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->nama }}</h5></strong></div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Jenis Kelamin</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ ($dokumen->jns_kel == 'P')?'PEREMPUAN':'LAKI-LAKI' }}</h5></strong></div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tempat Lahir</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->tmp_lahir }}</h5></strong></div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tanggal Lahir</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ \Carbon\Carbon::parse($dokumen->tgl_lahir)->format('d F Y') }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>NIK</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->nik }}</h5></strong>
              </div>
            </div>
            <hr>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Nomor Paspor</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->no_paspor }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tanggal Diberikan Paspor</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->tgl_diterbitkan }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tanggal Habis Berlaku Paspor</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->tgl_hbs_berlaku }}</h5></strong>
              </div>
            </div>
            <hr>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Alamat Lengkap</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->alamat }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Provinsi</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->provinsi }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Kota/Kabupaten</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->kabupaten }}</h5></strong>          
             </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Kecamatan</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->kecamatan }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Kelurahan/Desa</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->kelurahan }}</h5></strong>
              </div>
            </div>
            <hr>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Email</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->email }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Nomor Handphone / Telepon</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->no_tlp }}</h5></strong>
              </div>
            </div>
          </div>
        </div>
      </div> <!--end of left side-->
      
      <div class="col-sm-6">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>C.&emsp;PENDIDIKAN</strong></div>
          <div class="panel-body">
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tempat Kuliah</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->tmp_kuliah }}</h5></strong>
             </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Status Mahasiswa</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->desc_status_mahasiswa }}</h5></strong>
             </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Jenjang</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->desc_jenjang }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Perguruan Tinggi</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->nm_universitas }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Fakultas</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->fakultas }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Jurusan</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->jurusan }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Lulus Tahun</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->thn_lulus }}</h5></strong>
              </div>
            </div>
            
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Alamat Perguruan Tinggi</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->alamat_universitas }}</h5></strong>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>D.&emsp;IELTS / TOEFL iBT</strong></div>
          <div class="panel-body">
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Nilai/Skor</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->skor }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Tanggal Test</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->tgl_test }}</h5></strong>
              </div>
            </div>
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>No. Identitas Kandidat</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->no_identitas_kandidat }}</h5></strong>
              </div>
            </div>
          </div>            
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>E.&emsp;TUJUAN KEDATANGAN SELAMA DI AUSTRALIA</strong></div>
          <div class="panel-body">
            <div class="form-group row">    
              <div class="col-sm-5"><h5><small>Deskripsi</small></h5></div>
              <div class="col-sm-7"><strong class="font-color-freelancer"><h5>{{ $dokumen->alasan_kedatangan }}</h5></strong>
              </div>
            </div>
          </div>
        </div>                     
      </div><!--end of right side-->
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading" style="background-color: #c1fff3;"><strong>F.&emsp;LAMPIRAN DOKUMEN</strong></div>
          <div class="panel-body">
            <div class="col-sm-6">
              <?php $no = 0;
                for ($i=0 ; $i<count($ms_lampiran)/2 ; $i++) {
                  $flag = 0;
                  for ($j=0 ; $j<count($list_lampiran) ; $j++) {
                    if ($ms_lampiran[$i]->kd_lampiran == $list_lampiran[$j]->kd_lampiran) {
                      $flag = 0;
                 ?>
                  <div class="row form-input" style="padding-bottom: 10px">
                    <div class="col-sm-8"><h5>{{ ($i+1) }}. &emsp; <i>{{ $ms_lampiran[$i]->desc_lampiran }}</i></h5></div>
                    <div class="col-sm-3"> 
                      <form action="/downloadDokumenPemohon" method="POST" style="margin-bottom: 0px;">           
                        <input type="submit" class="btn btn-sm btn-success quota-card-button" name="submit" value="LIHAT">
                        {{ csrf_field() }}
                        <input type="hidden" name="link" value="{{ $list_lampiran[$j]->link }}">
                        <input type="hidden" name="_method" value="POST">
                      </form>
                    </div>
                  </div>
              <?php break; }
                    else {
                      $flag++;
                    }
                    if($j+1 == count($list_lampiran) && $flag > 0) { ?>
                      <div class="row form-input" style="padding-bottom: 10px">
                        <div class="col-sm-8"><h5>{{ $i+1 }}. &emsp; <i>{{ $ms_lampiran[$i]->desc_lampiran }}</i></h5></div>
                        <div class="col-sm-3"> 
                          <input type="submit" class="btn btn-sm btn-default quota-card-button" name="submit" value="TIDAK ADA" readonly="read" disabled="disabled">
                        </div>
                      </div>
              <?php
                    break;
                    }
                    $no = $i + 1;
                  }
                }
              ?>  
            </div>
            <div class="col-sm-6">
              <?php
                for ($i=$no ; $i<count($ms_lampiran) ; $i++) {
                  $flag = 0;
                  for ($j=0 ; $j<count($list_lampiran) ; $j++) {
                    if ($ms_lampiran[$i]->kd_lampiran == $list_lampiran[$j]->kd_lampiran) {
                      $flag = 0;
                 ?>
                  <div class="row form-input" style="padding-bottom: 10px">
                    <div class="col-sm-8"><h5>{{ $i+1 }}. &emsp; <i>{{ $ms_lampiran[$i]->desc_lampiran }}</i></h5></div>
                    <div class="col-sm-3"> 
                      <form action="/downloadDokumenPemohon" method="POST" style="margin-bottom: 0px;">           
                        <input type="submit" class="btn btn-sm btn-success quota-card-button" name="submit" value="LIHAT">
                        {{ csrf_field() }}
                        <input type="hidden" name="link" value="{{ $list_lampiran[$j]->link }}">
                        <input type="hidden" name="_method" value="POST">
                      </form>
                    </div>
                  </div>
              <?php break; }
                    else {
                      $flag++;
                    }
                    if($j+1 == count($list_lampiran) && $flag > 0) { ?>
                      <div class="row form-input" style="padding-bottom: 10px">
                        <div class="col-sm-8"><h5>{{ $i+1 }}. &emsp; <i>{{ $ms_lampiran[$i]->desc_lampiran }}</i></h5></div>
                        <div class="col-sm-3"> 
                          <input type="submit" class="btn btn-sm btn-default quota-card-button" name="submit" value="TIDAK ADA" readonly="read" disabled="disabled">
                        </div>
                      </div>
              <?php 
                    break;
                    }
                  }
                }
              ?>  
            </div> 
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><strong>STATUS</strong></div>
          <div class="panel-body">
            <center>
              <div class="col-md-12 col-sm-12">
                <?php if ($activity == null) { ?>
                  <form action="/verifikasiDokumen" class="form-inline" method="post" style="margin-bottom: 0px;">
                    <button type="button" class="btn btn-lg btn-danger" data-toggle="modal" data-target="#tolakVerifikasi">TOLAK &emsp; <i class="fa fa-times-circle"></i></button>&emsp;
                    <button type="submit" class="btn btn-lg btn-success" name="submit">TERIMA &emsp; <i class="fa fa-check-circle"></i></button>
                    {{ csrf_field() }}
                    <input type="hidden" name="id_user" value="{{ $dokumen->id_user }}">
                    <input type="hidden" name="id_kuota_wawancara" value="{{ $dokumen->id_kuota_wawancara }}">
                    <input type="hidden" name="tgl_wawancara" value="{{ $dokumen->tgl_wawancara }}">
                    <input type="hidden" name="no_tiket" value="{{ $dokumen->no_tiket }}">
                    <input type="hidden" name="status" value="Y">
                    <input type="hidden" name="_method" value="POST">
                  </form>
                <?php } else {
                  if ($activity->status == 'Y') {
                ?>
                  <button type="button" class="btn btn-lg btn-success" disabled="disabled" readonly="readonly">VERIFIKASI DOKUMEN DITERIMA &emsp; <i class="fa fa-check-circle"></i></button>
                <?php } elseif ($activity->status == 'N') { ?>
                  <button type="button" class="btn btn-lg btn-danger" disabled="disabled" readonly="readonly">VERIFIKASI DOKUMEN DITOLAK &emsp; <i class="fa fa-times-circle"></i></button>
                <?php } ?>
                <hr>
                <table class="table borderless">
                  <tr>
                    <td><h4><small>Tanggal</small></h4></td>
                    <td><h5><strong>{{ \Carbon\Carbon::parse($activity->activity_at)->addHours(7)->format('d F Y') }}</strong></h5></td>
                  </tr>
                  <tr>
                    <td><h4><small>Pukul</small></h4></td>
                    <td><h5><strong>{{  \Carbon\Carbon::parse($activity->activity_at)->addHours(7)->format('H:i:s') }}</strong></h5></td>
                  </tr>
                  <tr>
                    <td><h4><small>Verifikator</small></h4></td>
                    <td><h5><strong>{{ $activity->name }}</strong></h5></td>
                  </tr>
                  <tr>
                    <td><h4><small>Alasan</small></h4></td>
                    <td><h5><strong>{{ ($activity->keterangan != null)?$activity->keterangan:'-' }}</strong></h5></td>
                  </tr>
                </table>
              <?php } ?>
              </div>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>
<div id="tolakVerifikasi" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="/verifikasiDokumen" method="post" style="margin-bottom: 0px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Alasan Penolakan</h4>
        </div>
        <div class="modal-body">
            {{ csrf_field() }}
            <textarea name="keterangan" class="form-control" rows="5" id="keterangan"></textarea>
            
            <input type="hidden" name="_method" value="POST">
        </div>
        <div class="modal-footer">
          <center>
            <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">BATAL</button> &emsp;
            {{ csrf_field() }}
            <input type="hidden" name="id_user" value="{{ $dokumen->id_user }}">
            <input type="hidden" name="id_kuota_wawancara" value="{{ $dokumen->id_kuota_wawancara }}">
            <input type="hidden" name="tgl_wawancara" value="{{ $dokumen->tgl_wawancara }}">
            <input type="hidden" name="no_tiket" value="{{ $dokumen->no_tiket }}">
            <input type="hidden" name="status" value="N">
            <input type="hidden" name="_method" value="POST">
            <button type="submit" class="btn btn-danger btn-lg" name="submit">TOLAK &emsp; <i class="fa fa-times-circle"></i></button>
          </center>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection